import {GET_BOOK} from "./../Types/type";
import axios from "axios";

export default params => async dispatch => {
    const res = await axios.get('http://fakerestapi.azurewebsites.net/api/Books');
    dispatch(getAsync(res.data));
};

const getAsync = res => ({
    type: GET_BOOK,
    payload: res
});


