import {GET_CAT} from "./../Types/type";
import axios from "axios";

export default params => async dispatch => {
    const res = await axios.get('https://newapi.directik.com/api/cat');
    dispatch(getAsync(res.data));
};

const getAsync = res => ({
    type: GET_CAT,
    payload: res
});

