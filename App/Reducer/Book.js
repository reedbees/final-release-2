import {GET_BOOK} from "../type/type";

export default (state = null , action) => {
    switch(action.type) {
        case GET_BOOK:
            state = action.payload;
            return {
                data : state,
                alert: false
            };

        default:
            return state; 
    }
}




// const initialState = {
//     foodList: []
//   }
  
//   const foodReducer = (state = initialState, action) => {
//     switch (action.type) {
//       case GET_BOOK:
//         return {
//           ...state,
//           foodList: state.foodList.concat({
//             key: Math.random(),
//             name: action.data
//           })
//         };
//       default:
//         return state;
//     }
//   }
  