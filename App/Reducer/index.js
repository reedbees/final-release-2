import {combineReducers} from "redux"
import BookReducer from "./Book"

const rootReducer = combineReducers({
    book : BookReducer
});

export default rootReducer;