import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView} from "react-native"

import React from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"



export default class ps4 extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }


    componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("name");
        infos[1] = this.props.navigation.getParam("picLinks");
        infos[2] = this.props.navigation.getParam("price");
        infos[3] = this.props.navigation.getParam("description");

        return infos;
    }

    render() {
        return <ScrollView style={styles.Page}>
            <View style={styles.title}>
                <Text style={styles.Digi}>Digikala</Text>
            </View>

            <Text style={styles.kala}>{this.componentDidMount()[0]}</Text>

            <TouchableOpacity onPress={() => {
                const {navigate} = this.props.navigation;
                navigate("page3");
            }}><Text style={styles.ListApi}>See the list</Text></TouchableOpacity>

           

        </ScrollView>
        
    }
}


const styles = StyleSheet.create({

    ListApi: {
        marginLeft:responsiveWidth(30),
        marginTop:responsiveHeight(25),
        fontSize:responsiveFontSize(4),
    },

    Page: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: 'white',
    },

    title: {
        flexDirection: "row",
        backgroundColor:"red",
        height:responsiveHeight(10),
        width:responsiveWidth(100),
    },

    Digi: {
        color: "white",
        fontSize: responsiveFontSize(5),
        marginLeft:responsiveWidth(63)
    },

    kala: {
        color: 'black',
        textAlign: "center",
        fontSize: responsiveFontSize(5),
        marginTop:responsiveHeight(10),
    },

    card: {
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(67),
        marginBottom: responsiveHeight(3),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        // width
        marginLeft: responsiveWidth(4),
        fontSize: responsiveFontSize(2),
        color: "#EBEBEB"
    },

    articleImg: {
        width: responsiveWidth(100),
        height: responsiveHeight(100),
    }

})
