import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"

import { bindActionCreators } from "redux"
import axios from "axios"




class courseCreator extends React.Component {

    state = {
        title: '',
        payPerClass: "",
        cheker: true,
        payPerMounth: "",
        mainId: "",
        auth: "",
        status: 0,
        idSubject: "",
        titleSubject: ""
    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        infos[2] = this.props.navigation.getParam("id");
        infos[3] = this.props.navigation.getParam("title");
        alert(infos[1]);
        this.setState({ mainId: infos[1], auth: infos[0], idSubject: infos[2], titleSubject: infos[3] });
    }
    formPoster3 = () => {
        var formData = new FormData();
        formData.append("title", this.state.title);
        formData.append("payPerClass", this.state.payPerClass);
        formData.append("payPerMonth", this.state.payPerMounth);
        formData.append("subject", this.state.idSubject);
        formData.append("teacher",this.state.mainId);

        axios({
            method: 'POST',
            url: 'http://teacher.redbees.ir/teacher/course/create',
            data: formData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data;',
                "Authorization": this.state.auth
            }
        }).then((response) => { console.log(response.status); this.setState({ status: response.status }) })
            .catch(function (error) {
                console.log(error.response)
            });
    }

    showActivityList = () => {
        let infos = [];

        if (this.state.status == 200) {
            infos.push(
                <View style={styles.stat}>
                    <Image style={styles.container2} source={require("../../assets/yeap.png")} />
                    <Text style={styles.rashford}>ساختن درس با موفقیت انجام شد</Text>


                    <TouchableOpacity onPress={() => {
                        const { navigate } = this.props.navigation;
                        navigate("page25", {
                            auth: this.state.auth,
                            mainId: this.state.mainId,
                        });
                    }}>
                        <Image style={styles.rooney} source={require("../../assets/Go.png")} />
                    </TouchableOpacity>




                </View>
            );

        } else {

            if (this.state.cheker) {

                infos.push(
                    // <View>
                    //     <Image  source={require("../../assets/cr.png")} />
                    //     <Image  source={require("../../assets/vie.png")} />
                    // </View>

                    <View>


                        <TouchableOpacity onPress={() => {
                            this.setState({ cheker: false })
                        }}>
                            <View style={styles.khaf1}>
                                <Text style={styles.inside2}>ساختن درس جدید</Text>
                            </View>

                        </TouchableOpacity>




                        <TouchableOpacity onPress={() => {
                        const { navigate } = this.props.navigation;
                        navigate("page42", {
                            auth: this.state.auth,
                            mainId: this.state.mainId,
                        });
                    }}>
                        <View style={styles.khaf2}>
                            <Text style={styles.inside2}>دیدن بقیه درس ها </Text>
                        </View>
                        
                    </TouchableOpacity>


                        
                    </View>


                );

            } else {
                infos.push(

                    <View>
                        <View style={styles.articleBody23}>
                            <View style={styles.Page}>
                               
                                <View style={styles.container}>
                                    <TextInput
                                        value={this.state.title}

                                        placeholder={'موضوع'}
                                        style={styles.input}
                                        onChangeText={text => this.setState({ title: text })}
                                    />
                                </View>
                            </View>
                        </View>


                        <View style={styles.articleBody2}>
                            <View style={styles.Page}>
                                <View style={styles.container}>
                                    <TextInput
                                        value={this.state.payPerClass}
                                        placeholder={'هزینه بر هر کلاس'}
                                        style={styles.input}
                                        onChangeText={text => this.setState({ payPerClass: text })}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.articleBody2}>
                            <View style={styles.Page}>
                                <View style={styles.container}>
                                    <TextInput
                                        value={this.state.payPerMounth}
                                        placeholder={'هزینه بر ماه اجباری نیست!'}
                                        style={styles.input}
                                        onChangeText={text => this.setState({ payPerMounth: text })}
                                    />
                                </View>
                            </View>
                        </View>





                        <TouchableOpacity onPress={() => {
                            this.formPoster3();
                        }}>

                            <Image style={styles.inside1} source={require("../../assets/FuckYes.png")} />
                        </TouchableOpacity>

                    </View>

                );

            }
        }





        return infos;
    }



    render() {
        return <View>


            <View style={styles.Page2}>

                {this.showActivityList()}

            </View>




            <View style={styles.articleBody22}>
                {/* <Image style={styles.articleImg} source={require("../../assets/Group2.png")}/> */}
                <Image style={styles.articleImg} source={require("../../assets/exams.png")} />
                <Image style={styles.articleImg} source={require("../../assets/course.png")} />
                <Image style={styles.articleImg} source={require("../../assets/profile.png")} />
            </View>
        </View>


    }
}


const styles = StyleSheet.create({
    rashford: {
        fontFamily: "Poppins-Bold",
        fontSize: responsiveFontSize(3.5),
        marginTop: responsiveHeight(5),
        marginRight: responsiveWidth(3)
    },
    rooney: {
        marginLeft: responsiveWidth(40),
        marginTop: responsiveHeight(5)
    },
    stat: {
        width: responsiveWidth(100),
        height: responsiveHeight(100),
        backgroundColor: "#fdf7f0"
    },
    khaf1: {
        width: responsiveWidth(100),
        height: responsiveHeight(45),
        backgroundColor: "#68b712"
    },
    khaf2: {
        width: responsiveWidth(100),
        height: responsiveHeight(45),
        backgroundColor: "#ff8d2a"
    },
    Page: {
        flexDirection: "column",
        width: responsiveWidth(90)
    },
    articleBody2: {
        flexDirection: "row",
        marginTop: responsiveHeight(2),
        height: responsiveHeight(10),
    },
    articleBody23: {
        flexDirection: "row",
        marginTop: responsiveHeight(10),
        height: responsiveHeight(10),
    },
    khat: {
        marginLeft: responsiveWidth(14),
        marginTop: responsiveHeight(1)
    },
    col: {
        flexDirection: "column"
    },
    outer: {
        flexDirection: "row"
    },

    TextTitle: {
        fontSize: 20,
        color: "white",
    },

    container: {
        // marginTop: responsiveHeight(2.5),
        // marginLeft: responsiveWidth(8)
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',

    },
    container2: {
        marginTop: responsiveHeight(10),
        marginLeft: responsiveWidth(15)

    },

    Page2: {
        height: responsiveHeight(90),
        width: responsiveWidth(100),
        backgroundColor: '#285FD7',
    },
    TextTitle2: {
        fontSize: 35,

    },
    fonttext1: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(12),
        fontSize: responsiveFontSize(2.5),
        marginTop: responsiveHeight(0.5)
    },
    fonttext2: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(15),
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(0.5)

    },
    fonttext3: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(11),
        fontSize: responsiveFontSize(2.5),
        marginTop: responsiveHeight(0.5)

    },
    fonttext4: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginLeft: responsiveWidth(9),
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(0.5)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(15)

    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight: responsiveWidth(5),
    },

    inside1: {
        marginTop: responsiveHeight(15),
        marginLeft: responsiveWidth(35)
    },
    inside2: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(17),
        marginRight: responsiveWidth(13)


    },
    inside11: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(70)

    },
    inside12: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(65)
    },
    inside13: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(73)

    },

    outer2: {
        flexDirection: "column",
        width: responsiveWidth(42.5),
        backgroundColor: "#7ED321",
        height: responsiveHeight(10),
        marginLeft: responsiveWidth(5),
        borderRadius: 15,
        marginTop: responsiveHeight(2)
    },

    outer3: {
        flexDirection: "column",
        width: responsiveWidth(42.5),
        backgroundColor: "#ff8a25",
        height: responsiveHeight(10),
        marginLeft: responsiveWidth(5),
        borderRadius: 15,
        marginTop: responsiveHeight(2)
    },



    input: {
        width: responsiveWidth(80),
        height: 44,
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#ecf0f1'
    },


    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
        marginLeft: responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
    },
    card3: {
        fontFamily: "Poppins-Bold",
        color: "white",
        fontSize: responsiveFontSize(3),
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(1)
    },
    card4: {
        borderRadius: 85,
        height: responsiveHeight(20),
        width: responsiveWidth(40),
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(3)

    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop: responsiveHeight(2),
        height: responsiveHeight(10),
        color: "#f1f0f0"
    },
    articleBody22: {
        flexDirection: "row",
        height: responsiveHeight(10),
        backgroundColor: "#fff"
    },

    articleImg: {
        marginLeft: responsiveWidth(18.5),
        marginTop: responsiveHeight(1)
    },
    articleImgg: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10)
    },

    article2: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },
    articleImggg: {
        color: "#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height: responsiveHeight(8),
        marginTop: responsiveHeight(12),
        backgroundColor: "#285fd7"
    },
    textdemo: {
        color: "#ffffff",
        fontSize: responsiveFontSize(3),
        marginTop: responsiveHeight(1.5),
        marginLeft: responsiveWidth(22)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(courseCreator);
