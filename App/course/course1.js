import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput, Switch } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"
import * as Animatable from "react-native-animatable"
import Collapsible from "react-native-collapsible"
import Accordion from "react-native-collapsible/Accordion"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"

import { bindActionCreators } from "redux"
import Axios from "axios"
import DocumentPicker from "react-native-document-picker"
// const BACON_IPSUM =
//   'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs. Picanha beef prosciutto meatball turkey shoulder shank salami cupim doner jowl pork belly cow. Chicken shankle rump swine tail frankfurter meatloaf ground round flank ham hock tongue shank andouille boudin brisket. ';

// const CONTENT = [
//   {
//     title: 'First',
//     content: BACON_IPSUM,
//   },
//   {
//     title: 'Second',
//     content: BACON_IPSUM,
//   },
//   {
//     title: 'Third',
//     content: BACON_IPSUM,
//   },
//   {
//     title: 'Fourth',
//     content: BACON_IPSUM,
//   },
//   {
//     title: 'Fifth',
//     content: BACON_IPSUM,
//   },
// ];

// const SELECTORS = [
//   {
//     title: 'First',
//     value: 0,
//   },
//   {
//     title: 'Third',
//     value: 2,
//   },
//   {
//     title: 'None',
//   },
// ];




class course1 extends React.Component {

    state = {
        collapsed: true,
        aks: require("../../assets/Downarrow.png"),
        cheker: 0,
        auth : "",
        mainId : "",
        titleCourse : [],
        idCourse : [],
        titleSubject : [],
        teacherName : [],
        teacherLastName : [],
        teacherExperience : [],
        mobileTeacher : []

    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    toggleExpanded = () => {
        if (this.state.aks == require("../../assets/UpArrow.png")) {
            this.setState({ collapsed: !this.state.collapsed, aks: require("../../assets/Downarrow.png") });
        } else {
            this.setState({ collapsed: !this.state.collapsed, aks: require("../../assets/UpArrow.png") });
        }

    };








    componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        this.setState({auth : infos[0],mainId : infos[1]});
        alert(infos[1]);
        this.GetToken(infos[0],infos[1]);
    }



    showActivityList = () => {
        let infos = [];
        for (var i = 0; i < this.state.titleCourse.length; i++) {
            let a = this.state.titleCourse[i];
            let b = this.state.idCourse[i];
            let c = this.state.titleSubject[i];
            let d = this.state.teacherName[i];
            let e = this.state.teacherLastName[i];
            let f = this.state.teacherExperience[i];
            let g = this.state.mobileTeacher[i];
            
            infos.push(



                <View style={styles.container}>
                <ScrollView contentContainerStyle={{ paddingTop: 30 }}>




                    <TouchableOpacity onPress={this.toggleExpanded}>
                        <View style={styles.header}>
                            <View style={styles.outer}>
                                <View style={styles.layer20}>
                                    <View style={styles.outer}>
                                        <Text style={styles.headerText}>موضوع</Text>
                                        <Text style={styles.headerText2}>دسته</Text>
                                    </View>
                                    <View style={styles.outer}>
                                        <Text style={styles.headerText3}>{a}</Text>
                                        <Text style={styles.headerText4}>{c}</Text>
                                    </View>
                                </View>
                                <Image style={styles.marks} source={this.state.aks} />
                            </View>


                        </View>
                    </TouchableOpacity>
                    <Collapsible collapsed={this.state.collapsed} align="center">
                        <View style={styles.content}>

                            <Text>
                               
                             این کلاس با استاد {d}  {e} با تجربه {f} سال است
                           با شماره تلفن
                           {g} 
          </Text>
                        </View>
                    </Collapsible>




                </ScrollView>
            </View>





                );
        }
        return infos;
    }

    GetToken=(auth,input)=>{
    

        fetch('http://teacher.redbees.ir/student/classes/' + input , {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: auth,
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log('response didan : ',responseJson[0].course.teacher.mobileNumber);
                this.porkon(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    porkon(response) {
        for(var i = 0;i < response.length;i++) {
          var titleC = this.state.titleCourse.concat(response[i].course.title);
          var idcourse = this.state.idCourse.concat(response[i].course.id);
          var titleS = this.state.titleSubject.concat(response[i].course.subject.title);
          var teacherN = this.state.teacherName.concat(response[i].course.teacher.name);
          var teacherLN = this.state.teacherLastName.concat(response[i].course.teacher.lastName);
          var teacherE = this.state.teacherExperience.concat(response[i].course.teacher.yearsOfExperience);
          var mobilesh = this.state.mobileTeacher.concat(response[i].course.teacher.mobileNumber);
          this.setState({titleCourse : titleC, idCourse : idcourse , titleSubject : titleS,teacherName : teacherN,teacherLastName : teacherLN,teacherExperience : teacherE , mobileTeacher : mobilesh});
        }
  }






    render() {
        const { } = this.state;
        return <View style={styles.articleImggg}>
            <View style={styles.articleBody}>

                <View style={styles.textdemo}>

                    <View style={styles.row1}>


                        <TouchableOpacity onPress={() => {
                            const { navigate } = this.props.navigation;
                            navigate("page23");
                        }}>

                            <Image style={styles.card3} source={require("../../assets/notification.png")} />

                        </TouchableOpacity>



                    </View>

                    <View style={styles.row1}>
                        <Text style={styles.fontRow1}>My Course</Text>
                    </View>

                </View>



            </View>
            <ScrollView style={styles.rooney}>




                {this.showActivityList()}





            </ScrollView>

            <View style={styles.articleBody2}>
            <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page11");
    }}>  
              
              <Image style={styles.articleImg} source={require("../../assets/Group2.png")} />

            </TouchableOpacity>
                <Image style={styles.articleImg} source={require("../../assets/exams.png")} />
                <Image style={styles.articleImg} source={require("../../assets/courseB.png")} />
                <Image style={styles.articleImg} source={require("../../assets/profile.png")} />
            </View>


        </View>

    }
}


const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        paddingTop: 55,
    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: '#fff',
        // padding: responsiveHeight(2),
        width: responsiveWidth(90),
        marginRight: responsiveWidth(5),
        marginTop: responsiveHeight(5),
        height: responsiveHeight(10)
    },
    headerText: {
        fontSize: responsiveFontSize(2),
        fontWeight: '500',
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        marginLeft: responsiveWidth(1)
    },
    headerText3: {
        fontSize: responsiveFontSize(2),
        fontWeight: '500',
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        marginLeft: responsiveWidth(1),
        marginTop: responsiveHeight(1)
    },
    headerText2: {
        fontSize: responsiveFontSize(2),
        fontWeight: '500',
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        marginLeft: responsiveWidth(22)
    },
    headerText4: {
        fontSize: responsiveFontSize(2),
        fontWeight: '500',
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        marginLeft: responsiveWidth(22),
        marginTop: responsiveHeight(1)
    },
    content: {
        padding: 20,
        backgroundColor: '#fff',
        width: responsiveWidth(90)

    },
    active: {
        backgroundColor: 'rgba(255,255,255,1)',
    },
    inactive: {
        backgroundColor: 'rgba(245,252,255,1)',
    },
    selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    selector: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    activeSelector: {
        fontWeight: 'bold',
    },
    selectTitle: {
        fontSize: 14,
        fontWeight: '500',
        padding: 10,
    },
    multipleToggle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 30,
        alignItems: 'center',
    },
    multipleToggle__title: {
        fontSize: 16,
        marginRight: 8,
    },
    apr2019: {
        width: 63,
        height: 18,
        color: '#62656b',
        fontFamily: 'Poppins - Regular',
        fontSize: 12,
        fontWeight: '400',
        marginLeft: responsiveWidth(20)
    },
    namos: {
        marginTop: responsiveHeight(-13)
    },
    sub: {
        height: 18,
        color: '#62656b',
        fontFamily: 'Poppins - Regular',
        fontSize: responsiveFontSize(1.5),
        fontWeight: '400',
        marginTop: responsiveHeight(7),
        marginLeft: responsiveWidth(15)
    },
    sub2: {
        height: 18,
        color: '#62656b',
        fontFamily: 'Poppins - Regular',
        fontSize: responsiveFontSize(1.5),
        fontWeight: '400',
        marginTop: responsiveHeight(7),
        marginLeft: responsiveWidth(25)
    },
    science: {
        width: 64,
        height: 23,
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        fontSize: 16,
        fontWeight: '500',
        marginTop: responsiveHeight(11),
        marginLeft: responsiveWidth(-40)
    },
    science2: {
        width: 64,
        height: 23,
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        fontSize: 16,
        fontWeight: '500',
        marginTop: responsiveHeight(11),
        marginLeft: responsiveWidth(23)

    },
    marks: {
        marginTop: responsiveHeight(3),
        marginLeft: responsiveWidth(5)
    },
    layer20: {
        flexDirection: "column",
        width: responsiveWidth(70),
        backgroundColor: "#fff",
        marginTop: responsiveHeight(2),
        marginLeft: responsiveHeight(2)
    },
    zist: {
        width: 135,
        height: 26,
        color: '#24272a',
        fontFamily: 'Poppins - Semi Bold',
        fontSize: responsiveFontSize(2.4),
        marginLeft: responsiveWidth(12),
        marginTop: responsiveHeight(1)

    },
    startNow: {

        color: '#ff8418',
        fontFamily: 'Poppins - Bold',
        fontSize: responsiveFontSize(2.5),
        fontWeight: '700',
        marginLeft: responsiveWidth(25),
        marginTop: responsiveHeight(1)
    },
    oval: {
        width: responsiveWidth(12),
        height: responsiveHeight(6),
        backgroundColor: '#ff8418',
    },

    TextTitle: {
        fontSize: 35,

    },
    rooney: {
        width: responsiveWidth(100),
        height: responsiveHeight(80)
    },
    fonttext1: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(5)

    },
    fonttext2: {
        fontSize: responsiveFontSize(1.5),

    },
    fonttext3: {
        fontSize: responsiveFontSize(2),
        marginRight: responsiveWidth(20)

    },
    fonttext4: {
        fontSize: responsiveFontSize(1.5),
        marginRight: responsiveWidth(20),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(2)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(15)

    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight: responsiveWidth(5),
    },

    inside1: {
        flexDirection: "column",
        width: responsiveWidth(40),
        height: responsiveHeight(0)

    },
    inside2: {
        flexDirection: "column",
        height: responsiveHeight(0)

    },
    outer: {
        flexDirection: "row",
    },

    outer2: {
        flexDirection: "row",
        marginTop: responsiveHeight(10)
    },

    container: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: -30,
        marginBottom: responsiveHeight(2)
    },

    input: {
        width: responsiveWidth(80),
        height: 44,
        padding: 10,
        marginTop: responsiveHeight(3),
        backgroundColor: '#ffffff',
        marginRight: responsiveWidth(10)
    },
    Page: {
        marginTop: responsiveHeight(17),
        marginLeft: responsiveWidth(0)
    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fdc254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        color: "#fff",
        fontSize: responsiveFontSize(2.5),
        marginLeft: responsiveWidth(4.5),
        marginTop: responsiveHeight(1)
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
        marginLeft: responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
    },
    card3: {
        marginLeft: responsiveWidth(85),
        marginTop: responsiveHeight(2)

    },
    card4: {
        borderRadius: 50,
        height: responsiveHeight(5),
        width: responsiveWidth(10),
        marginLeft: responsiveWidth(5)

    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        height: responsiveHeight(10),
        backgroundColor: "#fff"
    },
    articleBody2: {
        flexDirection: "row",
        height: responsiveHeight(10),
        backgroundColor: "#fff"
    },

    articleImg: {
        marginLeft: responsiveWidth(13.5)
    },
    articleImgg: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10)
    },

    article2: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },
    articleImggg: {
        color: "#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height: responsiveHeight(8),
        marginTop: responsiveHeight(12),
        backgroundColor: "#285fd7"
    },
    textdemo: {
        flexDirection: "column"
    },
    row1: {
        flexDirection: "row"
    },
    fontRow1: {
        marginLeft: responsiveWidth(2),
        fontSize: responsiveFontSize(2.5),
        fontWeight: 'bold'
    },
    rectangle: {
        width: responsiveWidth(80),
        height: responsiveHeight(30),
        shadowColor: 'rgba(0, 0, 0, 0.09)',
        shadowOffset: { width: 9, height: 0 },
        shadowRadius: 10,
        borderRadius: 3,
        borderColor: '#eae7e4',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: '#ffffff',
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(course1);
