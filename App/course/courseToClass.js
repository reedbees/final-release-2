import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput, Alert, Button } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"
import axios from "axios"

import { bindActionCreators } from "redux"
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';




class courseToClass extends React.Component {




    state = {
        username: ''
    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['درس مورد تدریس', 'هزینه بر هر کلاس'],
            tableData: [
                ['ریاضی', '200'],
                ['برنامه نویسی جاوا', '150'],
                ['کردن', '300']
            ],
            tableHead1: ['درس مورد تدریس', 'هزینه بر هر ماه'],
            tableData1: [
                ['ریاضی', '2000'],
                ['برنامه نویسی جاوا', '1500'],
                ['کردن', '3000']
            ],
            tableEnd: ['هزینه کلی', '20000'],

            tableHead3: ['درس مورد تدریس', 'هزینه بر سال'],
            tableData3: [
                ['ریاضی', '20000'],
                ['برنامه نویسی جاوا', '15000'],
                ['کردن', '30000']
            ],
            tableEnd1: ['هزینه کلی', '200000'],
            auth: "",
            mainId: "",
            id: "",
            title: "",
            payPerClass: "",
            payPerMonth: "",
            teacher: "",
            mobileNumber: ""
        }
    }

    componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        infos[2] = this.props.navigation.getParam("id");
        infos[3] = this.props.navigation.getParam("title");
        infos[4] = this.props.navigation.getParam("payPerClass");
        infos[5] = this.props.navigation.getParam("payPerMonth");
        infos[6] = this.props.navigation.getParam("teacher");
        infos[7] = this.props.navigation.getParam("mobileNumber");
        this.setState({ auth: infos[0], mainId: infos[1], id: infos[2], title: infos[3], payPerClass: infos[4], payPerMonth: infos[5], teacher: infos[6], mobileNumber: infos[7] });
        this.setState({
            tableData: [
                [infos[4], infos[3]]
            ], tableData1: [[infos[5], infos[3]]]
        });
        this.setState({ tableEnd: ['هزینه کلی', infos[5]] });
        this.setState({
            tableData3: [
                [infos[5] * 12, infos[3]]
            ],
        });
        this.setState({ tableEnd1: ['هزینه کلی', infos[5] * 12] })



    }


    formPoster3 = () => {
        var formData = new FormData();
        formData.append("student", this.state.mainId);
        formData.append("course", this.state.id);

        axios({
            method: 'POST',
            url: 'http://teacher.redbees.ir/student/class/submit',
            data: formData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data;',
                "Authorization": this.state.auth
            }
        }).then((response) => {
            console.log("salam");
            console.log(response);
        })
            .catch(function (error) {
                console.log(error.response)
            });
    }



    render() {
        const state = this.state;
        return <View style={styles.TextTitle}>
            <View style={styles.articleBody}>


                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page11");
                }}>
                    <Image style={styles.card} source={require("../../assets/left-arrow(10).png")} />


                </TouchableOpacity>


                <Image style={styles.card2} source={require("../../assets/filter.png")} />
                <Image style={styles.card3} source={require("../../assets/notification.png")} />
            </View>


            <View style={styles.Digi}>

                <Image style={styles.card4} source={require("../../assets/mard1B.png")} />



                <View style={styles.kala2}>

                    <Text style={styles.kala}>{this.state.teacher.name}   {this.state.teacher.lastName}</Text>
                    <Text style={styles.kala3}>{this.state.title}</Text>
                </View>

            </View>

            <ScrollView horizontal={true} style={styles.category_container}>

                <View style={styles.articleImggg}>
                    <Text style={styles.TextInside3}>تجربه</Text>
                    <Text style={styles.Texter}>{this.state.teacher.yearsOfExperience}</Text>
                </View>

                <View style={styles.art3}>
                    <Text style={styles.TextInside2}>شماره‌تلفن</Text>
                    <Text style={styles.Texter}>{this.state.mobileNumber}</Text>
                </View>


                <View style={styles.art4}>
                    <Text style={styles.TextInside}>دانشگاه</Text>
                    <Text style={styles.Texter2}>{this.state.teacher.university}</Text>
                </View>


            </ScrollView>



            <ScrollView style={styles.Page}>








                <View style={styles.container}>
                    <Table borderStyle={{ borderWidth: 2, borderColor: '#fffff6' }}>
                        <Row data={state.tableHead} style={styles.head} textStyle={styles.text} />
                        <Rows data={state.tableData} textStyle={styles.text} />
                    </Table>



                </View>

                <View style={styles.container}>
                    <Table borderStyle={{ borderWidth: 2, borderColor: '#fffff6' }}>
                        <Row data={state.tableHead1} style={styles.head} textStyle={styles.text} />
                        <Rows data={state.tableData1} textStyle={styles.text} />
                        <Row data={state.tableEnd} style={styles.head} textStyle={styles.text} />
                    </Table>



                </View>

                <View style={styles.container}>
                    <Table borderStyle={{ borderWidth: 2, borderColor: '#fffff6' }}>
                        <Row data={state.tableHead3} style={styles.head} textStyle={styles.text} />
                        <Rows data={state.tableData3} textStyle={styles.text} />
                        <Row data={state.tableEnd1} style={styles.head} textStyle={styles.text} />
                    </Table>



                </View>

                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page11", {
                        auth: this.state.auth,
                        mainId: this.state.mainId,
                    });
                    this.formPoster3();
                }}>
                    <Image style={styles.demo} source={require("../../assets/bookdemo.png")} />


                </TouchableOpacity>



            </ScrollView>

            <View style={styles.title}>


                <Image style={styles.articleImg2} source={require("../../assets/Group2.png")} />
                <Image style={styles.articleImg2} source={require("../../assets/exams.png")} />
                <Image style={styles.articleImg2} source={require("../../assets/course.png")} />
                <Image style={styles.articleImg2} source={require("../../assets/profile.png")} />






            </View>

        </View>

    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
        paddingTop: 30,
        backgroundColor: '#f1f0f0'
    },

    head: {
        height: 40, backgroundColor: '#fffffa'
    },
    text: {
        margin: 6,
    },

    TextTitle: {
        backgroundColor: "#ffffff",
        width: responsiveWidth(100),
        height: responsiveHeight(100)
    },
    Texter: {
        color: "white",
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(1)
    },
    Texter2: {
        color: "white",
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(1),

    },

    TextInside: {
        color: "white",
        fontSize: responsiveFontSize(3),
        marginRight: responsiveWidth(8),
        marginTop: responsiveHeight(0.5)
    },
    TextInside3: {
        color: "white",
        fontSize: responsiveFontSize(3),
        marginRight: responsiveWidth(12),
        marginTop: responsiveHeight(0.5)
    },

    TextInside2: {
        color: "white",
        fontSize: responsiveFontSize(2),
        marginRight: responsiveWidth(10),
        marginTop: responsiveHeight(1)
    },

    category_container: {
        height: responsiveHeight(20),
        flexDirection: "row",
        backgroundColor: "#ffffff"

    },
    Page: {
        height: responsiveHeight(50),
        width: responsiveWidth(100),
        backgroundColor: "#f1f0f0"
    },

    Page2: {
        width: responsiveWidth(100),
        height: responsiveHeight(0),
        backgroundColor: "#ffffff"
    },
    title: {
        width: responsiveWidth(100),
        height: responsiveHeight(10),
        flexDirection: "row"
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        height: responsiveHeight(8),
        backgroundColor: "#ffffff",
        flexDirection: "row",
        marginTop: responsiveHeight(3)
    },

    kala: {
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(1),

    },
    kala3: {
        fontSize: responsiveFontSize(1.5),
        marginTop: responsiveHeight(1),

    },

    kala2: {
        height: responsiveHeight(5),
        flexDirection: "column",
        marginLeft: responsiveWidth(35)

    },

    card4: {
        borderRadius: 60,
        height: responsiveHeight(7),
        width: responsiveWidth(12),
        marginLeft: responsiveWidth(5)
    },
    card: {
        marginLeft: responsiveWidth(5),
        marginTop: responsiveHeight(1)
    },
    card2: {
        marginLeft: responsiveWidth(70),
        marginTop: responsiveHeight(1)
    },
    card3: {
        marginLeft: responsiveWidth(5),
        marginTop: responsiveHeight(0.5)
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        height: responsiveHeight(5),
        backgroundColor: "#fffff9"
    },

    articleImg: {
        marginLeft: responsiveWidth(33),
        marginTop: responsiveHeight(10),
    },
    articleImg2: {
        marginLeft: responsiveHeight(7),
        marginTop: responsiveHeight(1)
    },
    articleImgg: {
        marginLeft: responsiveWidth(33),
        marginTop: responsiveHeight(10)

    },
    articleImggg: {
        width: responsiveWidth(35),
        height: responsiveHeight(10),
        backgroundColor: "#3473dd",
        marginTop: responsiveHeight(5),
        marginLeft: responsiveWidth(5),
        borderRadius: 25
    },
    art3: {
        width: responsiveWidth(35),
        height: responsiveHeight(10),
        backgroundColor: "#ffa14f",
        marginTop: responsiveHeight(5),
        marginLeft: responsiveWidth(5),
        borderRadius: 25
    },
    art4: {
        width: responsiveWidth(35),
        height: responsiveHeight(10),
        backgroundColor: "#101316",
        marginTop: responsiveHeight(5),
        marginLeft: responsiveWidth(5),
        borderRadius: 25
    },
    demo: {
        marginLeft: responsiveWidth(27)
    }

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(courseToClass);
