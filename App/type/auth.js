import {ADD_AUTH} from "./type"


export const addAuth = (auth) => (
    {
        type : ADD_AUTH,
        data : auth
    }
);