import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView,AppRegistry, TextInput, Alert, Button} from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import {connect} from "react-redux"

import {bindActionCreators} from "redux"
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import Axios from "axios"


 class test2 extends React.Component {

    showAlert1() {  
        Alert.alert(  
            'Alert Title',  
            'My Alert Msg',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'OK', onPress: () => console.log('OK Pressed')},  
            ]  
        );  
    }  
    showAlert2() {  
        Alert.alert(  
            'Alert Title',  
            'My Alert Msg',  
            [  
                {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'OK', onPress: () => console.log('OK Pressed')},  
            ],  
            {cancelable: false}  
        )  
    }  

    state = {
        username: ''
      };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['درس مورد تدریس', 'هزینه'],
            tableData: [
              ['ریاضی', '200'],
              ['برنامه نویسی جاوا', '150'],
              ['کردن', '300']
            ]
          }
    }

    componentDidMount() {

        //  const bookss = await Axios.get('http://teacher.redbees.ir//getall');
        // alert(bookss.data[0].title);


        
        //let infos = [];
        // infos[0] = this.props.navigation.getParam("name");
        // infos[1] = this.props.navigation.getParam("desc");
        // infos[2] = this.props.navigation.getParam("sum");
        // infos[3] = this.props.navigation.getParam("publ");
        // infos[4] = this.props.navigation.getParam("pri");
        // infos[5] = this.props.navigation.getParam("id");
        // infos[6] = "http://reactnative.redbees.ir/books/file/" + this.props.navigation.getParam("id");
        // alert(this.props.books.data[0].PublishDate);
        

        // return infos;
    }



    render() {
        const state = this.state;
    return (
        <View style={styles.container} >  
        <Image style={styles.container} source={require("../../assets/khaf.png")} />
        <Text style={styles.buttonContainer}>ممنونیم!</Text>
        <Text style={styles.multiButtonContainer}>درخواست شما با موفقیت ارسال شد </Text>
        <Text style={styles.TextTitle}>۲۴ ساعت منتظر ما بمانید</Text>



        <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page11");
    }}>  
              
              
              <Image style={styles.Digi} source={require("../../assets/Done.png")} />

            </TouchableOpacity>


        
    </View>  
    )
        
    }
}


const styles = StyleSheet.create({

    container: {  
        marginLeft:responsiveWidth(11),
        marginTop:responsiveHeight(5)
    },  
    buttonContainer: {  
        fontSize:responsiveFontSize(5),
        marginRight:responsiveWidth(34),
        marginTop:responsiveHeight(5)
    },  
    multiButtonContainer: {  
        fontSize:responsiveFontSize(2.5),
        marginRight:responsiveWidth(15),
        marginTop:responsiveHeight(2)
    }  ,

    TextTitle: {
        fontSize: responsiveFontSize(2.5),
        marginRight:responsiveWidth(25)
       
    },
    fonttext1: {
        fontSize: responsiveFontSize(2),
        marginLeft:responsiveWidth(5)
       
    },
    fonttext2: {
        fontSize: responsiveFontSize(1.5),
       
    },
    fonttext3: {
        fontSize: responsiveFontSize(2),
        marginRight:responsiveWidth(20)
       
    },
    fonttext4: {
        fontSize: responsiveFontSize(1.5),
        marginRight:responsiveWidth(20),
        marginLeft:responsiveWidth(15),
        marginTop:responsiveHeight(2)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft:responsiveWidth(15)
       
    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight:responsiveWidth(5),     
    },

    inside1: {
        flexDirection:"column",
        width:responsiveWidth(40),
        height:responsiveHeight(0)
         
    },
    inside2: {
        flexDirection:"column",
        height:responsiveHeight(0)
       
    },
    outer: {
        flexDirection:"row",  
    },

    outer2: {
        flexDirection:"row",
        marginTop:responsiveHeight(10)  
    },

    
    Page: {
        width: responsiveWidth(100),
        height:responsiveHeight(78),
        backgroundColor:"#f1f0f0"
    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fdc254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: { 
        marginTop:responsiveHeight(15),
        marginLeft:responsiveWidth(25)
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop:responsiveHeight(1),
        marginLeft:responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5), 
        borderColor: 'gray', 
        borderWidth: 1,
        width:responsiveWidth(70), 
        marginLeft:responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
       marginLeft:responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
     },
     card3: {
        marginLeft:responsiveWidth(5),
        marginTop: -2
     },
     card4: {
        borderRadius: 50,
        height:responsiveHeight(5),
        width:responsiveWidth(10),
        marginLeft:responsiveWidth(5)
        
     },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop:responsiveHeight(2),
        height:responsiveHeight(10),
        color:"#f1f0f0"
    },
    articleBody2: {
        flexDirection: "row",
        height:responsiveHeight(10),
    },

    articleImg: {
        marginLeft:responsiveWidth(13.5)
    },
    articleImgg: {
        flexDirection:"column",
        width:responsiveWidth(80),
        backgroundColor:"#ffffff",
        height:responsiveHeight(30),
        marginLeft:responsiveWidth(10)
    },

    article2: {
        flexDirection:"column",
        width:responsiveWidth(80),
        backgroundColor:"#ffffff",
        height:responsiveHeight(30),
        marginLeft:responsiveWidth(10),
        marginTop:responsiveHeight(5)
    },
    articleImggg: {
        color:"#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height:responsiveHeight(8),
        marginTop:responsiveHeight(12),
        backgroundColor:"#285fd7"
    },
    textdemo: {
        color:"#ffffff",
        fontSize:responsiveFontSize(3),
        marginTop:responsiveHeight(1.5),
        marginLeft:responsiveWidth(22)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
			Getbook: getbook
			//GetCat:getcat
        },
        dispatch
    );

    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(test2);
    