import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"

import { bindActionCreators } from "redux"
import Axios from "axios"



class test2 extends React.Component {

    state = {
        id: [],
        title: [],
        date: [],
        totalMark: [],
        subject: [],
        finish: [],
        auth : "",
        mainId : ""
    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    async componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        this.setState({ mainId: infos[1], auth: infos[0] });
        infos[2] = 'http://teacher.redbees.ir/student/' +  'exams/' + infos[1] 
        


        const bookss = await Axios.get(infos[2], {
  headers: {
    'Authorization': infos[0]
  }
});


        // const bookss = await Axios.get('http://teacher.redbees.ir/exam');
        

        for (var i = 0; i < bookss.data.length; i++) {
            var id2 = this.state.id.concat(bookss.data[i].id);
            var title2 = this.state.title.concat(bookss.data[i].title);
            var data2 = this.state.date.concat(bookss.data[i].date);
            var subject2 = this.state.subject.concat(bookss.data[i].aclass.course.subject.title);
            var totalMark2 = this.state.totalMark.concat(bookss.data[i].totalPoint);
            var finish2 = this.state.finish.concat(bookss.data[i].finished)
            this.setState({ id: id2, title: title2, data: data2, subject: subject2, totalMark: totalMark2, finish: finish2 });
        }
    }

    showActivityList = () => {
        let infos = [];
        for (var i = 0; i < this.state.id.length; i++) {
            let a = this.state.id[i];
            let b = this.state.title[i];
            let c = this.state.date[i];
            let d = this.state.subject[i];
            let e;
            if (this.state.finish[i]) {
                e = <Image style= {styles.apr2019} source={require("../../assets/completed.png")} />
            } else {
                e = <Image style={styles.apr2019} source={require("../../assets/yettostart.png")} />
            }


            infos.push(
                <View>
                    <View style={styles.outer}>
                    <View style={styles.cole}>
                    <View style={styles.row}>
                        <Text style={styles.number2}>{a}</Text>
                        <Text style = {styles.basicAdditional}>{b}</Text>
                    </View>
                    </View>
                    <View style={styles.row}>
                        {e}
                    </View>

                    </View>
                    
                    
                    <Image style={styles.row} source={require("../../assets/rowdor.png")} />
                </View>
            );
        }
        return infos;
    }



    render() {
        return <View style={styles.articleImggg}>
            <View style={styles.articleBody}>

                <View style={styles.textdemo}>

                    <View style={styles.row1}>
                        <Image style={styles.card3} source={require("../../assets/notification.png")} />
                    </View>

                    <View style={styles.row1}>
                        <Text style={styles.fontRow1}>My Exams</Text>
                    </View>

                </View>



            </View>

            <ScrollView style={styles.rooney}>

                <Image style={styles.row} source={require("../../assets/rowdor.png")} />


                {this.showActivityList()}

            </ScrollView>

            <View style={styles.articleBody2}>
                <Image style={styles.articleImg} source={require("../../assets/Group2.png")} />
                <Image style={styles.articleImg} source={require("../../assets/examblue.png")} />
                <Image style={styles.articleImg} source={require("../../assets/course.png")} />
                <Image style={styles.articleImg} source={require("../../assets/profile.png")} />
            </View>


        </View>

    }
}


const styles = StyleSheet.create({
    cole : {
        flexDirection: "column",
        width : responsiveWidth(60)
    },
    basicAdditional: {
        
        color: '#686a71',
        fontFamily: 'Poppins - Regular',
        fontSize: responsiveFontSize(2.5),
        marginLeft : responsiveWidth(25),
        fontWeight: '400',
        marginTop : responsiveHeight(3)
      },
    number2: {
        
        color: '#62656b',
        fontFamily: 'Poppins - Regular',
        fontSize: responsiveFontSize(2.5),
        fontWeight: '400',
        marginLeft : responsiveWidth(5),
        marginTop : responsiveHeight(3)
    },
    chapter: {

        color: '#24272a',
        fontFamily: 'Poppins - Semi Bold',
        fontSize: responsiveFontSize(3),
        fontWeight: '600',
        marginLeft: responsiveWidth(5)
    },
    row: {
        flexDirection: "row",
        backgroundColor: "#fff",
        height : responsiveHeight(10)

    },
    apr2019: {
        marginLeft : responsiveWidth(10),
        marginTop: responsiveHeight(3)
    },
    namos: {
        marginTop: responsiveHeight(-13)
    },
    sub: {
        height: 18,
        color: '#62656b',
        fontFamily: 'Poppins - Regular',
        fontSize: responsiveFontSize(1.5),
        fontWeight: '400',
        marginTop: responsiveHeight(7),
        marginLeft: responsiveWidth(15)
    },
    sub2: {
        height: 18,
        color: '#62656b',
        fontFamily: 'Poppins - Regular',
        fontSize: responsiveFontSize(1.5),
        fontWeight: '400',
        marginTop: responsiveHeight(7),
        marginLeft: responsiveWidth(25)
    },
    science: {
        width: 64,
        height: 23,
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        fontSize: 16,
        fontWeight: '500',
        marginTop: responsiveHeight(11),
        marginLeft: responsiveWidth(-40)
    },
    science2: {
        width: 64,
        height: 23,
        color: '#24272a',
        fontFamily: 'Poppins - Medium',
        fontSize: 16,
        fontWeight: '500',
        marginTop: responsiveHeight(11),
        marginLeft: responsiveWidth(23)

    },
    marks: {
        width: 36,
        height: 18,
        color: '#62656b',
        fontFamily: 'Poppins - Regular',
        fontSize: 12,
        fontWeight: '400',
        marginLeft: responsiveWidth(66)
    },
    layer20: {
        width: responsiveWidth(10),
        height: 26,
        color: '#285fd7',
        fontFamily: 'Poppins - Bold',
        fontSize: 18,
        fontWeight: '700',
        marginLeft: responsiveWidth(70),
        marginTop: responsiveHeight(-5)
    },
    zist: {
        width: 135,
        height: 26,
        color: '#24272a',
        fontFamily: 'Poppins - Semi Bold',
        fontSize: responsiveFontSize(2.4),
        marginLeft: responsiveWidth(12),
        marginTop: responsiveHeight(1)

    },
    startNow: {

        color: '#ff8418',
        fontFamily: 'Poppins - Bold',
        fontSize: responsiveFontSize(2.5),
        fontWeight: '700',
        marginLeft: responsiveWidth(25),
        marginTop: responsiveHeight(1)
    },
    oval: {
        width: responsiveWidth(12),
        height: responsiveHeight(6),
        backgroundColor: '#ff8418',
    },

    TextTitle: {
        fontSize: 35,

    },
    rooney: {
        width: responsiveWidth(100),
        height: responsiveHeight(80),
        backgroundColor: "#fff"
    },
    fonttext1: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(5)

    },
    fonttext2: {
        fontSize: responsiveFontSize(1.5),

    },
    fonttext3: {
        fontSize: responsiveFontSize(2),
        marginRight: responsiveWidth(20)

    },
    fonttext4: {
        fontSize: responsiveFontSize(1.5),
        marginRight: responsiveWidth(20),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(2)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(15)

    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight: responsiveWidth(5),
    },

    inside1: {
        flexDirection: "column",
        width: responsiveWidth(40),
        height: responsiveHeight(0)

    },
    inside2: {
        flexDirection: "column",
        height: responsiveHeight(0)

    },
    outer: {
        flexDirection: "row",
    },

    outer2: {
        flexDirection: "row",
        marginTop: responsiveHeight(10)
    },

    container: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: -30,
        marginBottom: responsiveHeight(2)
    },

    input: {
        width: responsiveWidth(80),
        height: 44,
        padding: 10,
        marginTop: responsiveHeight(3),
        backgroundColor: '#ffffff',
        marginRight: responsiveWidth(10)
    },
    Page: {
        marginTop: responsiveHeight(17),
        marginLeft: responsiveWidth(0)
    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fdc254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        color: "#fff",
        fontSize: responsiveFontSize(2.5),
        marginLeft: responsiveWidth(4.5),
        marginTop: responsiveHeight(1)
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
        marginLeft: responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
    },
    card3: {
        marginLeft: responsiveWidth(85),
        marginTop: responsiveHeight(2)

    },
    card4: {
        borderRadius: 50,
        height: responsiveHeight(5),
        width: responsiveWidth(10),
        marginLeft: responsiveWidth(5)

    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        height: responsiveHeight(10),
        backgroundColor: "#fff"
    },
    articleBody2: {
        flexDirection: "row",
        height: responsiveHeight(10),
        backgroundColor: "#fff"
    },

    articleImg: {
        marginLeft: responsiveWidth(13.5)
    },
    articleImgg: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10)
    },

    article2: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },
    articleImggg: {
        color: "#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height: responsiveHeight(8),
        marginTop: responsiveHeight(12),
        backgroundColor: "#285fd7"
    },
    textdemo: {
        flexDirection: "column"
    },
    row1: {
        flexDirection: "row"
    },
    fontRow1: {
        marginLeft: responsiveWidth(2),
        fontSize: responsiveFontSize(2.5),
        fontWeight: 'bold'
    },
    rectangle: {
        width: responsiveWidth(80),
        height: responsiveHeight(30),
        shadowColor: 'rgba(0, 0, 0, 0.09)',
        shadowOffset: { width: 9, height: 0 },
        shadowRadius: 10,
        borderRadius: 3,
        borderColor: '#eae7e4',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: '#ffffff',
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(test2);
