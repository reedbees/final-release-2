import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"

import { bindActionCreators } from "redux"
import axios from "axios"



class mainExamCreator extends React.Component {

    state = {
        title: '',
        auth: "",
        mainId: "",
        year: "",
        month: "",
        day: "",
        status : 0,
        idExam : "",
        aclass : ""
    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        infos[2] = this.props.navigation.getParam("id");
        infos[3] = this.props.navigation.getParam("title");
        this.setState({ auth: infos[0], mainId: infos[1] , aclass : infos[2]});
        alert("پس از ساختن آزمون به صفحه طرح سوال منتقل خواهید شد");
    }


    formPoster3 = () => {
        var formData = new FormData();
        formData.append("title", this.state.title);
        formData.append("year", this.state.year);
        formData.append("month", this.state.month);
        formData.append("day", this.state.day);
        formData.append("aClass",this.state.aclass);

        axios({
            method: 'POST',
            url: 'http://teacher.redbees.ir/teacher/exam/create',
            data: formData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data;',
                "Authorization": this.state.auth
            }
        }).then((response) => { console.log(response.data.id); this.setState({status : response.status, idExam : response.data.id}); 
            // alert(response.data);        
    })
            .catch(function (error) {
                console.log(error.response)
            });
    }



    showActivityList = () => {
        let infos = [];

        if (this.state.status == 0) {
            infos.push(
                <View style={styles.Page2}>

                <View style={styles.container}>
                    <TextInput
                        value={this.state.title}
                        onChangeText={text => this.setState({ title: text })}
                        placeholder={'نام آزمون'}
                        style={styles.input}
                    />
                </View>

                <View style={styles.container}>
                    <TextInput
                        value={this.state.year}
                        onChangeText={text => this.setState({ year: text })}
                        placeholder={'سال'}
                        style={styles.input}
                    />
                </View>

                <View style={styles.container}>
                    <TextInput
                        value={this.state.month}
                        onChangeText={text => this.setState({ month: text })}
                        placeholder={'ماه'}
                        style={styles.input}
                    />
                </View>

                <View style={styles.container}>
                    <TextInput
                        value={this.state.day}
                        onChangeText={text => this.setState({ day: text })}
                        placeholder={'روز'}
                        style={styles.input}
                    />
                </View>

               


                <TouchableOpacity onPress={() => {
                            this.formPoster3();
                        }}>

<Image style={styles.maskhareBazi} source={require("../../assets/FuckYes.png")} />
                        </TouchableOpacity>

                </View>
            

            );

        } if(this.state.status == 200) {
            infos.push(


                <View style={styles.stat}>
                    <Image style={styles.container2} source={require("../../assets/yeap.png")} />
                    <Text style={styles.rashford}>ساختن آزمون با موفقیت انجام شد</Text>


                    <TouchableOpacity onPress={() => {
                        this.setState({status : 1000});
                    }}>
                        <Image style={styles.rooney} source={require("../../assets/Go.png")} />
                    </TouchableOpacity>




                </View>

            );

        } if(this.state.status == 1000) {
            infos.push(
                <View>
                    

                    <TouchableOpacity onPress={() => {
                        const { navigate } = this.props.navigation;
                        navigate("page33", {
                            auth: this.state.auth,
                            mainId: this.state.mainId,
                            idExam : this.state.idExam
                        });
                    }}>
                        <View style={styles.nareng}>
                        <Text style={styles.redFont}> طراحی سوالات</Text>
                    </View>
                        
                    </TouchableOpacity>




                <View style={styles.banafsh}>
                    <Text style={styles.redFont}>صفحه اصلی</Text>
                </View>
                <View style={styles.redd}>
                    <Text style={styles.redFont}>مشاهد بقیه آزمون ها</Text>
                </View>

                </View>
            );
        }


        return infos;
    }



    render() {
        return <View>




        {this.showActivityList()}
    




            <View style={styles.articleBody2}>
                {/* <Image style={styles.articleImg} source={require("../../assets/Group2.png")}/> */}
                <Image style={styles.articleImg} source={require("../../assets/exams.png")} />
                <Image style={styles.articleImg} source={require("../../assets/course.png")} />
                <Image style={styles.articleImg} source={require("../../assets/profile.png")} />
            </View>
        </View>



    }
}


const styles = StyleSheet.create({
    rashford: {
        fontFamily: "Poppins-Bold",
        fontSize: responsiveFontSize(3.5),
        marginTop: responsiveHeight(5),
        marginRight: responsiveWidth(5)
    },
    rooney: {
        marginLeft: responsiveWidth(37),
        marginTop: responsiveHeight(5)
    },
    stat: {
        width: responsiveWidth(100),
        height: responsiveHeight(100),
        backgroundColor: "#fdf7f0"
    },
    maskhareBazi : {
        marginLeft : responsiveWidth(35),
        marginTop : responsiveHeight(10)
    },
    redFont: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(10),
        marginRight: responsiveWidth(23)
    },
    nareng: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
        backgroundColor: "#e96a2e"
    },
    banafsh: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
        backgroundColor: "#dc3474"
    },
    redd: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
        backgroundColor: "#46080b"
    },
    khat: {
        marginLeft: responsiveWidth(14),
        marginTop: responsiveHeight(1)
    },
    col: {
        flexDirection: "column"
    },
    outer: {
        flexDirection: "row"
    },

    TextTitle: {
        marginLeft: responsiveWidth(75),
        marginTop: responsiveHeight(3)
    },
    Page2: {
        height: responsiveHeight(90),
        width: responsiveWidth(100),
        backgroundColor: '#285FD7',
    },
    TextTitle2: {
        fontSize: 35,

    },
    fonttext1: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(12),
        fontSize: responsiveFontSize(2.5),
        marginTop: responsiveHeight(0.5)
    },
    fonttext2: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(15),
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(0.5)

    },
    fonttext3: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(11),
        fontSize: responsiveFontSize(2.5),
        marginTop: responsiveHeight(0.5)

    },
    fonttext4: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginLeft: responsiveWidth(9),
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(0.5)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(15)

    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight: responsiveWidth(5),
    },

    inside1: {
        marginTop: responsiveHeight(4),
        marginLeft: responsiveWidth(5)
    },
    inside2: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(60)

    },
    inside11: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(70)

    },
    inside12: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(65)
    },
    inside13: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(73)

    },

    outer2: {
        flexDirection: "column",
        width: responsiveWidth(42.5),
        backgroundColor: "#7ED321",
        height: responsiveHeight(10),
        marginLeft: responsiveWidth(5),
        borderRadius: 15,
        marginTop: responsiveHeight(2)
    },

    outer3: {
        flexDirection: "column",
        width: responsiveWidth(42.5),
        backgroundColor: "#ff8a25",
        height: responsiveHeight(10),
        marginLeft: responsiveWidth(5),
        borderRadius: 15,
        marginTop: responsiveHeight(2)
    },



    container: {
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(10)
    },
    container2: {
        marginTop: responsiveHeight(10),
        marginLeft: responsiveWidth(15)

    },
    input: {
        width: responsiveWidth(70),
        height: 44,
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#ecf0f1',
        marginRight: responsiveWidth(15)
    },
    Page: {
        width: responsiveWidth(100),
        height: responsiveHeight(78),
        backgroundColor: "#f1f0f0"
    },


    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
        marginLeft: responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
    },
    card3: {
        fontFamily: "Poppins-Bold",
        color: "white",
        fontSize: responsiveFontSize(3),
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(1)
    },
    card4: {
        borderRadius: 85,
        height: responsiveHeight(20),
        width: responsiveWidth(40),
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(3)

    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop: responsiveHeight(2),
        height: responsiveHeight(10),
        color: "#f1f0f0"
    },
    articleBody2: {
        flexDirection: "row",
        height: responsiveHeight(10),
        backgroundColor: "#fff"
    },

    articleImg: {
        marginLeft: responsiveWidth(18.5),
        marginTop: responsiveHeight(1)
    },
    articleImgg: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10)
    },

    article2: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },
    articleImggg: {
        color: "#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height: responsiveHeight(8),
        marginTop: responsiveHeight(12),
        backgroundColor: "#285fd7"
    },
    textdemo: {
        color: "#ffffff",
        fontSize: responsiveFontSize(3),
        marginTop: responsiveHeight(1.5),
        marginLeft: responsiveWidth(22)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(mainExamCreator);
