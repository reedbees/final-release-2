import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"

import { bindActionCreators } from "redux"




class changeExams extends React.Component {

    state = {
        username: '',
        auth: "",
        mainId: ""
    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        this.setState({ auth: infos[0], mainId: infos[1] });

    }



    render() {
        return <View>


            <View style={styles.Page2}>




                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page32", {
                        auth: this.state.auth,
                        mainId: this.state.mainId
                    });
                }}>
                    <View style={styles.nareng}>
                        <Text style={styles.redFont}>تغییر سوالات</Text>
                    </View>

                </TouchableOpacity>



                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page35", {
                        auth: this.state.auth,
                        mainId: this.state.mainId
                    });
                }}>

                    <View style={styles.redd}>
                        <Text style={styles.redFont}>پاک کردن آزمون</Text>
                    </View>
                </TouchableOpacity>



            </View>




            <View style={styles.articleBody2}>
                {/* <Image style={styles.articleImg} source={require("../../assets/Group2.png")}/> */}
                <Image style={styles.articleImg} source={require("../../assets/exams.png")} />
                <Image style={styles.articleImg} source={require("../../assets/course.png")} />
                <Image style={styles.articleImg} source={require("../../assets/profile.png")} />
            </View>
        </View>



    }
}


const styles = StyleSheet.create({
    redFont: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(18),
        marginRight: responsiveWidth(23)
    },
    nareng: {
        width: responsiveWidth(100),
        height: responsiveHeight(45),
        backgroundColor: "#e96a2e"
    },
    banafsh: {
        width: responsiveWidth(100),
        height: responsiveHeight(30),
        backgroundColor: "#dc3474"
    },
    redd: {
        width: responsiveWidth(100),
        height: responsiveHeight(45),
        backgroundColor: "#46080b"
    },
    khat: {
        marginLeft: responsiveWidth(14),
        marginTop: responsiveHeight(1)
    },
    col: {
        flexDirection: "column"
    },
    outer: {
        flexDirection: "row"
    },

    TextTitle: {
        marginLeft: responsiveWidth(75),
        marginTop: responsiveHeight(3)
    },

    container: {
        marginTop: responsiveHeight(2.5),
        marginLeft: responsiveWidth(8)

    },

    Page2: {
        height: responsiveHeight(90),
        width: responsiveWidth(100),
        backgroundColor: '#285FD7',
    },
    TextTitle2: {
        fontSize: 35,

    },
    fonttext1: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(12),
        fontSize: responsiveFontSize(2.5),
        marginTop: responsiveHeight(0.5)
    },
    fonttext2: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(15),
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(0.5)

    },
    fonttext3: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginRight: responsiveWidth(11),
        fontSize: responsiveFontSize(2.5),
        marginTop: responsiveHeight(0.5)

    },
    fonttext4: {
        fontFamily: "Poppins-Regular",
        color: "#fff",
        marginLeft: responsiveWidth(9),
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(0.5)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(15)

    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight: responsiveWidth(5),
    },

    inside1: {
        marginTop: responsiveHeight(4),
        marginLeft: responsiveWidth(5)
    },
    inside2: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(60)

    },
    inside11: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(70)

    },
    inside12: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(65)
    },
    inside13: {
        fontFamily: "Poppins-SemiBold",
        color: "#fff",
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(3.5),
        marginLeft: responsiveWidth(73)

    },

    outer2: {
        flexDirection: "column",
        width: responsiveWidth(42.5),
        backgroundColor: "#7ED321",
        height: responsiveHeight(10),
        marginLeft: responsiveWidth(5),
        borderRadius: 15,
        marginTop: responsiveHeight(2)
    },

    outer3: {
        flexDirection: "column",
        width: responsiveWidth(42.5),
        backgroundColor: "#ff8a25",
        height: responsiveHeight(10),
        marginLeft: responsiveWidth(5),
        borderRadius: 15,
        marginTop: responsiveHeight(2)
    },



    input: {
        width: responsiveWidth(80),
        height: 44,
        padding: 10,
        marginTop: responsiveHeight(3),
        backgroundColor: '#ffffff',
        marginRight: responsiveWidth(10)
    },
    Page: {
        width: responsiveWidth(100),
        height: responsiveHeight(78),
        backgroundColor: "#f1f0f0"
    },


    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
        marginLeft: responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
    },
    card3: {
        fontFamily: "Poppins-Bold",
        color: "white",
        fontSize: responsiveFontSize(3),
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(1)
    },
    card4: {
        borderRadius: 85,
        height: responsiveHeight(20),
        width: responsiveWidth(40),
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(3)

    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop: responsiveHeight(2),
        height: responsiveHeight(10),
        color: "#f1f0f0"
    },
    articleBody2: {
        flexDirection: "row",
        height: responsiveHeight(10),
        backgroundColor: "#fff"
    },

    articleImg: {
        marginLeft: responsiveWidth(18.5),
        marginTop: responsiveHeight(1)
    },
    articleImgg: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10)
    },

    article2: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },
    articleImggg: {
        color: "#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height: responsiveHeight(8),
        marginTop: responsiveHeight(12),
        backgroundColor: "#285fd7"
    },
    textdemo: {
        color: "#ffffff",
        fontSize: responsiveFontSize(3),
        marginTop: responsiveHeight(1.5),
        marginLeft: responsiveWidth(22)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(changeExams);
