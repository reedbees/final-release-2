import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView,AppRegistry, TextInput} from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import {connect} from "react-redux"

import {bindActionCreators} from "redux"
import axios from "axios"




 class test2 extends React.Component {

    state = {
        title: '',
        auth : "",
        mainId : ""
      };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props);
        
    }

    componentDidMount() {

        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        alert(infos[0]);
        this.setState({ mainId: infos[1], auth: infos[0] });
        
        alert("لطفا ابتدا موضوع مورد نظر خود را جستجو کنید در صورت عدم وجود بسازید");
        

        
    }

    updateValue(text,field) {
        console.warn(text);
    }

    formPoster = () => {
        let body = new FormData();
    let formData = new FormData();
    formData.append('title', this.state.title);

fetch("http://teacher.redbees.ir/subject/create",
  {
      body: formData,
      method: "post"
  });

    }

    formPoster3 = () => {
        var formData = new FormData();
        formData.append("title", this.state.title);
        axios({
            method: 'POST',
            url: 'http://teacher.redbees.ir/subject/create',
            data: formData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data;',
                "Authorization": this.state.auth
            }
        }).then( (response) =>{ console.log(response);  })
            .catch(function (error) {
                console.log(error.response)
            });
    }

    render() {
        return <ScrollView style={styles.Page2}>

            <Image style={styles.articleImg} source={require("../../assets/reading.png")} />
            <Text style={styles.TextTitle}>موضوع ساز!</Text>

            <View style={styles.container}>
       <TextInput
         value={this.state.username}
        
         placeholder={'موضوع'}
         style={styles.input}
         onChangeText={text => this.setState({ title: text })}

       />


     </View>

    


            <View style={styles.articleBody}>
            <TouchableOpacity onPress={() => { {this.formPoster3()};const {navigate} = this.props.navigation;
            navigate("page25",{
                auth : this.state.auth,
                mainId : this.state.mainId
            });
    }}>  
              <Image style={styles.articleImggg} source={require("../../assets/Submit.png")} />
               
            </TouchableOpacity>
            
      
            </View>
            



        </ScrollView>
        
    }
}


const styles = StyleSheet.create({

    TextTitle: {
        fontSize: 35,
        marginRight: responsiveWidth(34),
        color: "white"
    },

    container: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        
      },
      input: {
        width: responsiveWidth(70),
        height: 44,
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#ecf0f1',
        marginRight: responsiveWidth(15)
      },
      input: {
        width: responsiveWidth(70),
        height: 44,
        padding: 10,
        marginTop: responsiveHeight(3),
        backgroundColor: '#ecf0f1',
        marginRight: responsiveWidth(15)
      },
    Page: {
        fontSize: 25,
        marginRight: responsiveWidth(15)
    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fb1254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: { 
        height: responsiveHeight(5), 
        borderColor: 'gray', 
        borderWidth: 1,
        width:responsiveWidth(70), 
        marginLeft:responsiveWidth(15),
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop:responsiveHeight(1),
        marginLeft:responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5), 
        borderColor: 'gray', 
        borderWidth: 1,
        width:responsiveWidth(70), 
        marginLeft:responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
       marginLeft:responsiveWidth(15),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop:responsiveHeight(8),
        height:responsiveHeight(10),
    },

    articleImg: {
        marginLeft: responsiveWidth(33),
        marginTop: responsiveHeight(10),
    },
    articleImgg: {
        marginLeft: responsiveWidth(33),
        marginTop: responsiveHeight(10)

    },
    articleImggg: {
        marginLeft:responsiveWidth(35)
    }

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
			Getbook: getbook
			//GetCat:getcat
        },
        dispatch
    );

    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(test2);
    