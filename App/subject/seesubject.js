import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"

import { bindActionCreators } from "redux"
import Axios from "axios"



class test2 extends React.Component {

    state = {
        title: [],
        id: [],
        mainId: "",
        auth: "",
        response: []
    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        this.setState({ mainId: infos[1], auth: infos[0] });

        //alert(infos[0]);



        // const bookss = await Axios.get('http://teacher.redbees.ir/subject');
        //   for(var i = 0;i < bookss.data.length;i++) {
        //     var title2 = this.state.title.concat(bookss.data[i].title);
        //     var idd = this.state.id.concat(bookss.data[i].id);
        //     this.setState({title: title2,id: idd});
        //   }



        //console.log(this.state.auth);
        this.GetToken(infos[0]);


    }

    porkon(response) {
        for (var i = 0; i < response.length; i++) {
            var title2 = this.state.title.concat(response[i].title);
            var idd = this.state.id.concat(response[i].id);
            this.setState({ title: title2, id: idd });
        }
    }


    GetToken = (auth) => {


        fetch('http://teacher.redbees.ir/subject', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: auth,
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log('response', responseJson);
                this.porkon(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });
    };


    showActivityList = () => {
        let infos = [];
        for (var i = 0; i < this.state.id.length; i++) {
            let a = this.state.title[i];
            let f = this.state.id[i];


            infos.push(
                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page16", {
                        title: a,
                        id: f,
                        auth: this.state.auth,
                        mainId: this.state.mainId
                    });
                }}>

                    <View style={styles.blueBody}>
                        <Text style={styles.textdemo}>{this.state.title[i]}</Text>


                    </View>

                </TouchableOpacity>
            );
        }
        return infos;
    }



    render() {
        return <View style={styles.articleImggg}>
            <View style={styles.articleBody}>
                <Image style={styles.card} source={require("../../assets/left-arrow(10).png")} />
                <Text style={styles.moalm}>به معلم</Text>


                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page28", {
                        auth: this.state.auth,
                        mainId: this.state.mainId
                    });
                }}>

                    <Image style={styles.card2} source={require("../../assets/filter.png")} />

                </TouchableOpacity>




                <Image style={styles.card3} source={require("../../assets/notification.png")} />
            </View>
            <ScrollView style={styles.Page}>








                {this.showActivityList()}

















            </ScrollView>
            <View style={styles.articleBody2}>
                <Image style={styles.articleImg} source={require("../../assets/Group2.png")} />






                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page14");
                }}>

                    <Image style={styles.articleImg} source={require("../../assets/exams.png")} />

                </TouchableOpacity>


                <Image style={styles.articleImg} source={require("../../assets/course.png")} />
                <Image style={styles.articleImg} source={require("../../assets/profile.png")} />
            </View>


        </View>

    }
}


const styles = StyleSheet.create({
    moalm: {
        marginLeft: responsiveWidth(35),
        fontFamily: "Poppins-Medium",
        color: "#ff8418",
        fontSize: responsiveFontSize(2)
    },

    TextTitle: {
        fontSize: 35,

    },
    fonttext1: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(5)

    },
    fonttext2: {
        fontSize: responsiveFontSize(1.5),

    },
    fonttext3: {
        fontSize: responsiveFontSize(2),
        marginRight: responsiveWidth(20)

    },
    fonttext4: {
        fontSize: responsiveFontSize(1.5),
        marginRight: responsiveWidth(20),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(2)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(15)

    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight: responsiveWidth(5),
    },

    inside1: {
        flexDirection: "column",
        width: responsiveWidth(40),
        height: responsiveHeight(0)

    },
    inside2: {
        flexDirection: "column",
        height: responsiveHeight(0)

    },
    outer: {
        flexDirection: "row",
    },

    outer2: {
        flexDirection: "row",
        marginTop: responsiveHeight(10)
    },

    container: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: -30,
        marginBottom: responsiveHeight(2)
    },

    input: {
        width: responsiveWidth(80),
        height: 44,
        padding: 10,
        marginTop: responsiveHeight(3),
        backgroundColor: '#ffffff',
        marginRight: responsiveWidth(10)
    },
    Page: {
        width: responsiveWidth(100),
        height: responsiveHeight(78),
        backgroundColor: "#f1f0fb",

    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fdc254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
        marginLeft: responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(25)
    },
    card3: {
        marginLeft: responsiveWidth(5),
        marginTop: -2
    },
    card4: {
        borderRadius: 50,
        height: responsiveHeight(5),
        width: responsiveWidth(10),
        marginLeft: responsiveWidth(5)

    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop: responsiveHeight(2),
        height: responsiveHeight(10),
        color: "#f1f0f0"
    },
    articleBody2: {
        flexDirection: "row",
        height: responsiveHeight(10),
    },

    articleImg: {
        marginLeft: responsiveWidth(13.5)
    },
    articleImgg: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10)
    },

    article2: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },
    articleImggg: {
        color: "#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height: responsiveHeight(8),
        marginTop: responsiveHeight(2),
        backgroundColor: "#285fd7",
        width: responsiveWidth(80),
        marginLeft: responsiveWidth(10)
    },
    textdemo: {
        color: "#ffffff",
        fontSize: responsiveFontSize(3),
        marginTop: responsiveHeight(1.5),
        marginLeft: responsiveWidth(25),
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(test2);
