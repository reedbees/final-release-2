import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput } from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import { connect } from "react-redux"

import { bindActionCreators } from "redux"

import Axios from "axios"



class test2 extends React.Component {

    state = {
        id: [],
        name: [],
        lastname: [],
        yearsOfExperience: [],
        university: [],
        title: "",
        auth: "",
        mainId: "",
        cover : ""
    };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    async componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("title");
        infos[1] = this.props.navigation.getParam("id");
        infos[2] = this.props.navigation.getParam("auth");
        infos[3] = this.props.navigation.getParam("mainId");
        alert(infos[3]);
        this.setState({ auth: infos[2], mainId: infos[3] });


        this.GetToken(infos[2], 'http://teacher.redbees.ir/subject/teachers/' + infos[1],infos[0]);
        // const bookss = await Axios.get('http://teacher.redbees.ir/subject/teachers/' + infos[1]);
        // alert(bookss.data[0].name);
        // for (var i = 0; i < bookss.data.length; i++) {
        //     var id2 = this.state.id.concat(bookss.data[i].id);
        //     var name2 = this.state.name.concat(bookss.data[i].name);
        //     var lastname2 = this.state.lastname.concat(bookss.data[i].lastName);
        //     var yearsOfExperience2 = this.state.yearsOfExperience.concat(bookss.data[i].yearsOfExperience);
        //     var university2 = this.state.university.concat(bookss.data[i].university);
        //     this.setState({ id: id2, name: name2, lastname: lastname2, yearsOfExperience: yearsOfExperience2, university: university2, title: infos[0] });
        // }

        
    }

    porkon(response,infsef) {
        for (var i = 0; i < response.length; i++) {
            var id2 = this.state.id.concat(response[i].id);
            var name2 = this.state.name.concat(response[i].name);
            var lastname2 = this.state.lastname.concat(response[i].lastName);
            var yearsOfExperience2 = this.state.yearsOfExperience.concat(response[i].yearsOfExperience);
            var university2 = this.state.university.concat(response[i].university);
            var cov = this.state.cover.concat(response[i].cover);
            this.setState({ id: id2, name: name2, lastname: lastname2, yearsOfExperience: yearsOfExperience2, university: university2, title: infsef , cover : cov });
        }
    }

    GetToken = (auth, url,infsef) => {


        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: auth,
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log('response', responseJson);
                this.porkon(responseJson,infsef);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    showActivityList = () => {
        let infos = [];
        for (var i = 0; i < this.state.id.length; i++) {
            let a = this.state.id[i];
            let b = this.state.name[i];
            let c = this.state.lastname[i];
            let d = this.state.yearsOfExperience[i];
            let e = this.state.university[i];
            let f = this.state.cover[i];
            let Image_Http_URL = { uri: 'http://teacher.redbees.ir/teacher/file/' + this.state.id[i] };


            if(f == 1) {
                infos.push(
                    <View style={styles.article2}>
                        <View style={styles.outer}>
                            <View style={styles.inside1}>
    
                                <Image style={styles.card4} source={require("../../assets/zan1k.png")} />
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >{this.state.name[i]} {this.state.lastname[i]}</Text>
    
                                <Text style={styles.fonttext2} >{this.state.title}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
                            <View style={styles.inside1}>
                                <Text style={styles.fonttext3}>تجربه</Text>
                                <Text style={styles.fonttext4}>{this.state.yearsOfExperience[i]}</Text>
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >دانشگاه</Text>
                                <Text style={styles.fonttext5} >{this.state.university[i]}</Text>
                            </View>
                        </View>
                        <View style={styles.blueBody}>
                            <Text style={styles.textdemo}>Book a Demo</Text>
                        </View>
                    </View>
                );
            }
            if(f == 2) {
                infos.push(
                    <View style={styles.article2}>
                        <View style={styles.outer}>
                            <View style={styles.inside1}>
    
                                <Image style={styles.card4} source={require("../../assets/zan2k.png")} />
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >{this.state.name[i]} {this.state.lastname[i]}</Text>
    
                                <Text style={styles.fonttext2} >{this.state.title}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
                            <View style={styles.inside1}>
                                <Text style={styles.fonttext3}>تجربه</Text>
                                <Text style={styles.fonttext4}>{this.state.yearsOfExperience[i]}</Text>
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >دانشگاه</Text>
                                <Text style={styles.fonttext5} >{this.state.university[i]}</Text>
                            </View>
                        </View>
                        <View style={styles.blueBody}>
                            <Text style={styles.textdemo}>Book a Demo</Text>
                        </View>
                    </View>
                );
            }
            if(f == 3) {
                infos.push(
                    <View style={styles.article2}>
                        <View style={styles.outer}>
                            <View style={styles.inside1}>
    
                                <Image style={styles.card4} source={require("../../assets/mard1k.png")} />
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >{this.state.name[i]} {this.state.lastname[i]}</Text>
    
                                <Text style={styles.fonttext2} >{this.state.title}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
                            <View style={styles.inside1}>
                                <Text style={styles.fonttext3}>تجربه</Text>
                                <Text style={styles.fonttext4}>{this.state.yearsOfExperience[i]}</Text>
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >دانشگاه</Text>
                                <Text style={styles.fonttext5} >{this.state.university[i]}</Text>
                            </View>
                        </View>
                        <View style={styles.blueBody}>
                            <Text style={styles.textdemo}>Book a Demo</Text>
                        </View>
                    </View>
                );
            }
            if(f == 4) {
                infos.push(
                    <View style={styles.article2}>
                        <View style={styles.outer}>
                            <View style={styles.inside1}>
    
                                <Image style={styles.card4} source={require("../../assets/mard2k.png")} />
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >{this.state.name[i]} {this.state.lastname[i]}</Text>
    
                                <Text style={styles.fonttext2} >{this.state.title}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
                            <View style={styles.inside1}>
                                <Text style={styles.fonttext3}>تجربه</Text>
                                <Text style={styles.fonttext4}>{this.state.yearsOfExperience[i]}</Text>
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >دانشگاه</Text>
                                <Text style={styles.fonttext5} >{this.state.university[i]}</Text>
                            </View>
                        </View>
                        <View style={styles.blueBody}>
                            <Text style={styles.textdemo}>Book a Demo</Text>
                        </View>
                    </View>
                );
            }
            if(f == 5) {
                infos.push(
                    <View style={styles.article2}>
                        <View style={styles.outer}>
                            <View style={styles.inside1}>
    
                                <Image style={styles.card4} source={require("../../assets/mard3k.png")} />
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >{this.state.name[i]} {this.state.lastname[i]}</Text>
    
                                <Text style={styles.fonttext2} >{this.state.title}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
                            <View style={styles.inside1}>
                                <Text style={styles.fonttext3}>تجربه</Text>
                                <Text style={styles.fonttext4}>{this.state.yearsOfExperience[i]}</Text>
                            </View>
                            <View style={styles.inside2}>
                                <Text style={styles.fonttext1} >دانشگاه</Text>
                                <Text style={styles.fonttext5} >{this.state.university[i]}</Text>
                            </View>
                        </View>
                        <View style={styles.blueBody}>
                            <Text style={styles.textdemo}>Book a Demo</Text>
                        </View>
                    </View>
                );
            }
        }
        return infos;
    }

    render() {
        return <View style={styles.articleImggg}>
            <View style={styles.articleBody}>
                <Image style={styles.card} source={require("../../assets/left-arrow(10).png")} />

                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page15");
                }}>

                    <Image style={styles.card2} source={require("../../assets/filter.png")} />

                </TouchableOpacity>


                <Image style={styles.card3} source={require("../../assets/notification.png")} />
            </View>
            <ScrollView style={styles.Page}>






                {/* <Text>{this.showOb()[0]}</Text> */}












                {this.showActivityList()}


            </ScrollView>
            <View style={styles.articleBody2}>
                <Image style={styles.articleImg} source={require("../../assets/Group2.png")} />






                <TouchableOpacity onPress={() => {
                    const { navigate } = this.props.navigation;
                    navigate("page14");
                }}>

                    <Image style={styles.articleImg} source={require("../../assets/exams.png")} />

                </TouchableOpacity>


                <Image style={styles.articleImg} source={require("../../assets/course.png")} />
                <Image style={styles.articleImg} source={require("../../assets/profile.png")} />
            </View>


        </View>

    }
}


const styles = StyleSheet.create({

    TextTitle: {
        fontSize: 35,

    },
    fonttext1: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(5)

    },
    fonttext2: {
        fontSize: responsiveFontSize(2),
        marginTop: responsiveHeight(2),
        marginLeft: responsiveWidth(7)

    },
    fonttext3: {
        fontSize: responsiveFontSize(2),
        marginRight: responsiveWidth(20)

    },
    fonttext4: {
        fontSize: responsiveFontSize(1.5),
        marginRight: responsiveWidth(20),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(2)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft: responsiveWidth(15)

    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight: responsiveWidth(5),
    },

    inside1: {
        flexDirection: "column",
        width: responsiveWidth(40),
        height: responsiveHeight(0)

    },
    inside2: {
        flexDirection: "column",
        height: responsiveHeight(0)

    },
    outer: {
        flexDirection: "row",
    },

    outer2: {
        flexDirection: "row",
        marginTop: responsiveHeight(10)
    },

    container: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: -30,
        marginBottom: responsiveHeight(2)
    },

    input: {
        width: responsiveWidth(80),
        height: 44,
        padding: 10,
        marginTop: responsiveHeight(3),
        backgroundColor: '#ffffff',
        marginRight: responsiveWidth(10)
    },
    Page: {
        width: responsiveWidth(100),
        height: responsiveHeight(78),
        backgroundColor: "#f1f0f0"
    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fdc254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop: responsiveHeight(1),
        marginLeft: responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5),
        borderColor: 'gray',
        borderWidth: 1,
        width: responsiveWidth(70),
        marginLeft: responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
        marginLeft: responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
    },
    card3: {
        marginLeft: responsiveWidth(5),
        marginTop: -2
    },
    card4: {
        borderRadius: 50,
        height: responsiveHeight(5),
        width: responsiveWidth(10),
        marginLeft: responsiveWidth(5)

    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop: responsiveHeight(2),
        height: responsiveHeight(10),
        color: "#f1f0f0"
    },
    articleBody2: {
        flexDirection: "row",
        height: responsiveHeight(10),
    },

    articleImg: {
        marginLeft: responsiveWidth(13.5)
    },
    articleImgg: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10)
    },

    article2: {
        flexDirection: "column",
        width: responsiveWidth(80),
        backgroundColor: "#ffffff",
        height: responsiveHeight(30),
        marginLeft: responsiveWidth(10),
        marginTop: responsiveHeight(5)
    },
    articleImggg: {
        color: "#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height: responsiveHeight(8),
        marginTop: responsiveHeight(12),
        backgroundColor: "#285fd7"
    },
    textdemo: {
        color: "#ffffff",
        fontSize: responsiveFontSize(3),
        marginTop: responsiveHeight(1.5),
        marginLeft: responsiveWidth(22)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            Getbook: getbook
            //GetCat:getcat
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(test2);
