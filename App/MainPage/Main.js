import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView} from "react-native"

import React from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

 import Item from "../../Components/Item"

 


export default class Main extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    state = {
        names: ["Ps4", "iPhone 7", "macBook Pro 2019"],
        picLinks: [<Image style={styles.articleImg} source={require("../../assets/ps4.jpg")} />,
        <Image style={styles.articleImg} source={require("../../assets/iphone7.jpeg")} />,
        <Image style={styles.articleImg} source={require("../../assets/macbook16.jpg")} />],
        prices: [400, 599, 1799],
        descriptions: ["5000Gb", "128GB", "256GB core i5 RAM 8GB"],
    }

    sendItemsInfo = () => {
        let infos = [];
        for (var i = 0; i < this.state.names.length; i++) {
            let a = this.state.names.filter(item => item == this.state.names[i]);
            let b = this.state.picLinks.filter(item => item == this.state.picLinks[i]);
            let c = this.state.prices.filter(item => item == this.state.prices[i]);
            let d = this.state.descriptions.filter(item => item == this.state.descriptions[i]);

            infos.push(
                <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
                        navigate("page2",{
                            name:a,
                            picLink:b,
                            price:c,
                            description:d,
                        });
                }}>
                    <Item name={this.state.names[i]}
                        price={this.state.prices[i]}
                        picLink={this.state.picLinks[i]}
                        description={this.state.descriptions[i]}
                    />
                </TouchableOpacity>);
        }
        return infos;
    }


    render() {
        return <ScrollView style={styles.Page}>
            <View style={styles.title}>
                <Text style={styles.Digi}>Digikala</Text>
            </View>

            {this.sendItemsInfo()}

        </ScrollView>
        
    }
}


const styles = StyleSheet.create({

    Page: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: 'white',
    },

    title: {
        flexDirection: "row",
        backgroundColor:"red",
        height:responsiveHeight(10),
    },

    Digi: {
        color: "white",
        fontSize: responsiveFontSize(5),
        marginLeft:responsiveWidth(63)
    },

    kala: {
        color: '#FAFAFA',
        textAlign: "center",
        fontSize: responsiveFontSize(5),
    },

    card: {
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(67),
        marginBottom: responsiveHeight(3),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        // width
        marginLeft: responsiveWidth(4),
        fontSize: responsiveFontSize(2),
        color: "#EBEBEB"
    },

    articleImg: {
        marginRight: responsiveWidth(2),
        marginLeft: responsiveWidth(5),
        width: responsiveWidth(60),
        height: responsiveHeight(20),
    }

})
