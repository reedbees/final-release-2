import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView,AppRegistry} from "react-native"

import React from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"



import getbook from "./../../Action/get_book"

import {connect} from "react-redux"

import {bindActionCreators} from "redux"




 class test2 extends React.Component {

    state = {
        names: [],
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        //let infos = [];
        // infos[0] = this.props.navigation.getParam("name");
        // infos[1] = this.props.navigation.getParam("desc");
        // infos[2] = this.props.navigation.getParam("sum");
        // infos[3] = this.props.navigation.getParam("publ");
        // infos[4] = this.props.navigation.getParam("pri");
        // infos[5] = this.props.navigation.getParam("id");
        // infos[6] = "http://reactnative.redbees.ir/books/file/" + this.props.navigation.getParam("id");
        // alert(this.props.books.data[0].PublishDate);
        

        // return infos;
    }



    render() {
        return <ScrollView style={styles.Page2}>
            
           <Text style={styles.TextTitle}>خوش آمدید!</Text>
           <Text style={styles.Page}>شما دانش آموز هستید.</Text>


            <Image style={styles.articleImg} source={require("../../assets/checker.png")} />
            <Text style={styles.title}>دانش آموز</Text>

            {/* <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page5");
    }}>  
                <Image style={styles.articleImg} source={require("../../assets/reading.png")} />
               
            </TouchableOpacity> */}

<TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page9");
    }}>  
                <Image style={styles.articleImgg} source={require("../../assets/teacher.png")} />
               
            </TouchableOpacity>

          
            <Text style={styles.title2}>استاد</Text>

            {/* <Image style={styles.kala} source={require("../../assets/Go.png")} /> */}

            <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page6");
    }}>  
              <Image style={styles.kala} source={require("../../assets/Go.png")} />  
               
            </TouchableOpacity>


        </ScrollView>
        
    }
}


const styles = StyleSheet.create({

    TextTitle: {
        marginRight: responsiveWidth(30),
        fontSize: 35,
        marginTop: responsiveHeight(2),
        color: "white"
    },

    Page: {
        fontSize: 25,
        marginRight: responsiveWidth(25),
        color: "white"
    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fdc254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2),
        color: "white"
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2),
        color: "white"
    },
    Digi: {
        color: "white",
        fontSize: responsiveFontSize(5),
        marginLeft:responsiveWidth(63)
    },

    kala: {
        marginLeft: responsiveWidth(37),
        marginTop: responsiveHeight(2)
    },

    kala2: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(3),
        marginTop:responsiveHeight(3),
        marginLeft:responsiveWidth(3)
    },

    card: {
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(67),
        marginBottom: responsiveHeight(3),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        // width
        marginLeft: responsiveWidth(4),
        fontSize: responsiveFontSize(2),
        color: "#EBEBEB"
    },

    articleImg: {
        marginLeft: responsiveWidth(33),
        marginTop: responsiveHeight(10)
    },
    articleImgg: {
        marginLeft: responsiveWidth(33),
        marginTop: responsiveHeight(10)

    },
    articleImggg: {
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(5),
    }

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
			Getbook: getbook
			//GetCat:getcat
        },
        dispatch
    );

    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(test2);
    