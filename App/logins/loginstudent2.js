import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, AppRegistry, TextInput } from "react-native"

import React, { Component } from "react"

import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions"

import { Provider } from "react-redux"
import configureStore from "../store/store"
import getbook from "./../../Action/get_book"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"

import { addAuth } from "../type/auth"
import authReducer from "../store/authReducer"



import axios from "axios"



class loginstudent2 extends React.Component {

  state = {
    cover : "",
    id : "",
    color: "#fdc254",
    auth: "",
    username: '',
    password: '',
    status: <TouchableOpacity onPress={() => {
      this.hameChiRadife();
    }}>
      <Image style={styles.articleImggg} source={require("../../assets/Login.png")} />

    </TouchableOpacity>,
    status2: <TouchableOpacity onPress={() => {
      const { navigate } = this.props.navigation;
      navigate("page7");
    }}>
      <Image style={styles.card} source={require("../../assets/forgetpas.png")} />

    </TouchableOpacity>,
    img: <Image style={styles.articleImg} source={require("../../assets/reading.png")} />,
    signer : <TouchableOpacity onPress={() => {
      const { navigate } = this.props.navigation;
      navigate("page39");
    }}>
      <Image style={styles.signup} source={require("../../assets/signup.png")} />

    </TouchableOpacity>

  };

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state
  }

  constructor(props) {
    super(props)
  }

  async componentDidMount() {
    this.props.Getbook()
  }

  hameChiRadife = () => {
    var mahdi;
    const params = JSON.stringify({

      "username": this.state.username,

      "password": this.state.password,

    });
    axios.post("http://teacher.redbees.ir/student/login", params, {

      "headers": {

        "content-type": "application/json",

      },

    }).then((response) => {



      if (response.status == 200) {
        mahdi = 200;
        console.log("cover : " + response.data.cover);
        this.setState({ auth: response.data.token , cover : response.data.cover});
        //alert(this.state.auth);
      } else {
        mahdi = 401;
      }
      this.radifSaz(mahdi);



    })

      .catch(function (error) {

        console.log(error);

      });

  }

  radifSaz = (input) => {

    if (input == 200) {
      this.setState({
         img: <Image style={styles.articleImg} source={require("../../assets/checker.png")} />, status2: null, color: "#2766b5", signer : null
      });

        this.GetToken();

    }

  }

  GetToken=()=>{
    

    fetch('http://teacher.redbees.ir/student/mobileNumber/' + this.state.username, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: this.state.auth,
        }
    }).then((response) => response.json())
        .then((responseJson) => {
            console.log('response',responseJson.id);
            this.setState({ status: <TouchableOpacity onPress={() => {
              const { navigate } = this.props.navigation;
              navigate("page11", {
                auth: this.state.auth,
                mainId : responseJson.id,
                cover : this.state.cover
              });
    
            }}>
              <Image style={styles.onp} source={require("../../assets/logred.png")} />
    
            </TouchableOpacity>})
        })
        .catch((error) => {
            console.error(error);
        });
};




  render() {
    return <ScrollView style={[styles.Page2, { backgroundColor: this.state.color }]}>

      {this.state.img}

      {/* <Image style={styles.articleImg} source={require("../../assets/checker.png")} /> */}
      <Text style={styles.TextTitle}>!وارد شوید</Text>

      <View style={styles.container}>
        <TextInput
          value={this.state.username}
          onChangeText={text => this.setState({ username: text })}
          placeholder={'نام کاربری'}
          style={styles.input}
        />
      </View>

      <View style={styles.container}>
        <TextInput
          value={this.state.password}
          onChangeText={text => this.setState({ password: text })}
          placeholder={'رمز عبور'}
          style={styles.input}
        />
      </View>


      <View style={styles.articleBody}>


        {this.state.status2}


        {this.state.status}

        

      </View>

      {this.state.signer}




    </ScrollView>

  }
}


const styles = StyleSheet.create({
  signup : {
    marginLeft : responsiveWidth(30)
  },
  onp: {
    marginLeft: responsiveWidth(35)
  },

  TextTitle: {
    fontSize: 35,
    marginRight: responsiveWidth(34),
    color: "white"
  },

  container: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',

  },
  input: {
    width: responsiveWidth(70),
    height: 44,
    padding: 10,
    marginBottom: 10,
    backgroundColor: '#ecf0f1',
    marginRight: responsiveWidth(15)
  },
  input: {
    width: responsiveWidth(70),
    height: 44,
    padding: 10,
    marginTop: responsiveHeight(3),
    backgroundColor: '#ecf0f1',
    marginRight: responsiveWidth(15)
  },
  Page: {
    fontSize: 25,
    marginRight: responsiveWidth(15)
  },

  Page2: {
    height: responsiveHeight(100),
    width: responsiveWidth(100),
    // backgroundColor:  "#fdc254"

  },
  title: {
    fontSize: 25,
    marginRight: responsiveWidth(40),
    marginTop: responsiveHeight(2)
  },

  title2: {
    fontSize: 25,
    marginRight: responsiveWidth(45),
    marginTop: responsiveHeight(2)
  },
  Digi: {
    height: responsiveHeight(5),
    borderColor: 'gray',
    borderWidth: 1,
    width: responsiveWidth(70),
    marginLeft: responsiveWidth(15),
  },

  kala: {
    color: 'black',
    textAlign: "left",
    fontSize: responsiveFontSize(5),
    marginTop: responsiveHeight(1),
    marginLeft: responsiveWidth(3)
  },

  kala2: {
    height: responsiveHeight(5),
    borderColor: 'gray',
    borderWidth: 1,
    width: responsiveWidth(70),
    marginLeft: responsiveWidth(15),
    marginTop: responsiveHeight(5)
  },

  card: {
    marginLeft: responsiveWidth(15),
  },

  articleTitle: {
    // marginLeft: responsiveWidth(3),
    width: responsiveWidth(100),
    fontSize: responsiveFontSize(3),
    color: "white",
  },

  articleBody: {
    flexDirection: "row",
    marginTop: responsiveHeight(8),
    height: responsiveHeight(10),
  },

  articleImg: {
    marginLeft: responsiveWidth(33),
    marginTop: responsiveHeight(10),
  },
  articleImgg: {
    marginLeft: responsiveWidth(33),
    marginTop: responsiveHeight(10)

  },
  articleImggg: {
    marginLeft: responsiveWidth(5)
  }

})


const mapStateToProps = state => ({
  books: state.book
});

// const mapStateToProps = state => {
//     console.log(state);
//     return {
//         auths : state.authReducer.addAuth
//     }
// }


const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      Getbook: getbook

    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(loginstudent2);




