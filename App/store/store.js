import {createStore , combineReducers} from "redux"

import authReducer from "./authReducer"


const rootReducer = combineReducers({
    auth : authReducer
})

const configureStore = () => createStore(rootReducer);

export default configureStore;