import { ADD_AUTH } from "../../Types/type"

const initialState = {
    authToken: ""
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_AUTH:
            return{
                ...state,
                authToken : ({
                    name : action.data
                })
            };
        default:
            return state;
    }


}

export default authReducer;