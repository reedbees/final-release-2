import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView,AppRegistry, TextInput} from "react-native"

import React, { Component } from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Sae } from 'react-native-textinput-effects';

import getbook from "./../../Action/get_book"

import {connect} from "react-redux"

import {bindActionCreators} from "redux"
import axios from "axios"



 class test2 extends React.Component {

    state = {
        username: '',
        mainId : "",
        auth : "",
        cover : "",
        id : [],
        name : [],
        lastName : [],
        yearsOfExperience : [],
        university : [],
        coverT : [],
        aks : null
      };

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    async componentDidMount() {
        let infos = [];
        infos[0] = this.props.navigation.getParam("auth");
        infos[1] = this.props.navigation.getParam("mainId");
        infos[2] = this.props.navigation.getParam("cover");
        this.setState({mainId : infos[1], auth : infos[0] , cover : infos[2]});
        //this.setState({aks : <Image style={styles.babaGhori} source={require("../../assets/pesar1k.png")} />})
        if(infos[2] == 1) {
            this.setState({aks : <Image style={styles.babaGhori}  source={require("../../assets/dokh1k.png")} />})
        }
        if(infos[2] == 2) {
            this.setState({aks : <Image style={styles.babaGhori} source={require("../../assets/pesar1k.png")} />})
        }
        if(infos[2] == 3) {
            this.setState({aks : <Image style={styles.babaGhori} source={require("../../assets/pesar2k.png")} />})
        }
        if(infos[2] == 4) {
            this.setState({aks : <Image style={styles.babaGhori} source={require("../../assets/pesar3k.png")} />})
        }

        const bookss = await axios.get('http://teacher.redbees.ir');
        // id":1,"name":"Emad","lastName":"Ans","yearsOfExperience":"5","university":"Raf","point":0.0,"male":true,"cover":2
        for (var i = 0; i < bookss.data.length; i++) {
            var nam = this.state.name.concat(bookss.data[i].name);
            var idd = this.state.id.concat(bookss.data[i].id);
            var lnam = this.state.lastName.concat(bookss.data[i].lastName);
            var year = this.state.yearsOfExperience.concat(bookss.data[i].yearsOfExperience);
            var univer = this.state.university.concat(bookss.data[i].university);
            var cov = this.state.coverT.concat(bookss.data[i].cover);
            this.setState({name : nam , id : idd , lastName : lnam , yearsOfExperience : year , university : univer , coverT : cov});
        }
        console.log(bookss.data);

    }


    showActivityList = () => {
        let infos = [];
        for (var i = 0; i < this.state.id.length; i++) {
            let a = this.state.name[i];
            let b = this.state.lastName[i];
            let c = this.state.yearsOfExperience[i];
            let d = this.state.university[i];
            let e = this.state.coverT[i];
            let f = this.state.id[i];


            if(e == 1) {
                infos.push(
                    <View style={styles.article2}>
    
                <View style={styles.outer}>
    
    
                            <View style={styles.inside1}>
    
                                 <Image style={styles.card4} source={require("../../assets/zan1k.png")} />
                            </View>
                            <View style={styles.inside2}>
                <Text style={styles.fonttext1} >{a} {b}</Text>
                <Text style={styles.fonttext2} >محل تحصیل {d}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
    
    
                            <View style={styles.inside1}>
    
                                 <Text style={styles.fonttext3}>تجربه</Text>
                <Text style={styles.fonttext4}>{c}</Text>
                                 
                            </View>
    
    
                            <View style={styles.inside2}>
    
                            
                            </View>
                        </View>
                    <View style={styles.blueBody}>
    
    
    
    
                    <TouchableOpacity onPress={() => { alert("شما در حال نگاه کردن به اساتید برتر ما طی هفته گذشته هستید برا رزرو این اساتید باید به بخش موضوعات مراجعه کنید");
        }}>  
                  
                  <Text style={styles.textdemo}>Book a Demo</Text>
    
                </TouchableOpacity>
                        
    
    
    
                        
                    </View>
                </View>
                    
                );
            }
            if(e == 2) {
                infos.push(

                    <View style={styles.article2}>
    
                <View style={styles.outer}>
    
    
                            <View style={styles.inside1}>
    
                                 <Image style={styles.card4} source={require("../../assets/zan2k.png")} />
                            </View>
                            <View style={styles.inside2}>
                <Text style={styles.fonttext1} >{a} {b}</Text>
                <Text style={styles.fonttext2} >محل تحصیل {d}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
    
    
                            <View style={styles.inside1}>
    
                                 <Text style={styles.fonttext3}>تجربه</Text>
                <Text style={styles.fonttext4}>{c}</Text>
                                 
                            </View>
    
    
                            <View style={styles.inside2}>
    
                            
                            </View>
                        </View>
                    <View style={styles.blueBody}>
    
    
    
    
                    <TouchableOpacity onPress={() => { alert("شما در حال نگاه کردن به اساتید برتر ما طی هفته گذشته هستید برا رزرو این اساتید باید به بخش موضوعات مراجعه کنید");
        }}>  
                  
                  <Text style={styles.textdemo}>Book a Demo</Text>
    
                </TouchableOpacity>
                        
    
    
    
                        
                    </View>
                </View>
                    
                );
            }
            if(e == 3) {
                infos.push(

                    <View style={styles.article2}>
    
                <View style={styles.outer}>
    
    
                            <View style={styles.inside1}>
    
                                 <Image style={styles.card4} source={require("../../assets/mard1k.png")} />
                            </View>
                            <View style={styles.inside2}>
                <Text style={styles.fonttext1} >{a} {b}</Text>
                <Text style={styles.fonttext2} >محل تحصیل {d}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
    
    
                            <View style={styles.inside1}>
    
                                 <Text style={styles.fonttext3}>تجربه</Text>
                <Text style={styles.fonttext4}>{c}</Text>
                                 
                            </View>
    
    
                            <View style={styles.inside2}>
    
                            
                            </View>
                        </View>
                    <View style={styles.blueBody}>
    
    
    
    
                    <TouchableOpacity onPress={() => { alert("شما در حال نگاه کردن به اساتید برتر ما طی هفته گذشته هستید برا رزرو این اساتید باید به بخش موضوعات مراجعه کنید");
        }}>  
                  
                  <Text style={styles.textdemo}>Book a Demo</Text>
    
                </TouchableOpacity>
                        
    
    
    
                        
                    </View>
                </View>
                    
                );
            }
            if(e == 4) {
                infos.push(

                    <View style={styles.article2}>
    
                <View style={styles.outer}>
    
    
                            <View style={styles.inside1}>
    
                                 <Image style={styles.card4} source={require("../../assets/mard2k.png")} />
                            </View>
                            <View style={styles.inside2}>
                <Text style={styles.fonttext1} >{a} {b}</Text>
                <Text style={styles.fonttext2} >محل تحصیل {d}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
    
    
                            <View style={styles.inside1}>
    
                                 <Text style={styles.fonttext3}>تجربه</Text>
                <Text style={styles.fonttext4}>{c}</Text>
                                 
                            </View>
    
    
                            <View style={styles.inside2}>
    
                            
                            </View>
                        </View>
                    <View style={styles.blueBody}>
    
    
    
    
                    <TouchableOpacity onPress={() => { alert("شما در حال نگاه کردن به اساتید برتر ما طی هفته گذشته هستید برا رزرو این اساتید باید به بخش موضوعات مراجعه کنید");
        }}>  
                  
                  <Text style={styles.textdemo}>Book a Demo</Text>
    
                </TouchableOpacity>
                        
    
    
    
                        
                    </View>
                </View>
                    
                );
            }
            if(e == 5) {
                infos.push(

                    <View style={styles.article2}>
    
                <View style={styles.outer}>
    
    
                            <View style={styles.inside1}>
    
                                 <Image style={styles.card4} source={require("../../assets/mard3k.png")} />
                            </View>
                            <View style={styles.inside2}>
                <Text style={styles.fonttext1} >{a} {b}</Text>
                <Text style={styles.fonttext2} >محل تحصیل {d}</Text>
                            </View>
                        </View>
                        <View style={styles.outer2}>
    
    
                            <View style={styles.inside1}>
    
                                 <Text style={styles.fonttext3}>تجربه</Text>
                <Text style={styles.fonttext4}>{c}</Text>
                                 
                            </View>
    
    
                            <View style={styles.inside2}>
    
                            
                            </View>
                        </View>
                    <View style={styles.blueBody}>
    
    
    
    
                    <TouchableOpacity onPress={() => { alert("شما در حال نگاه کردن به اساتید برتر ما طی هفته گذشته هستید برا رزرو این اساتید باید به بخش موضوعات مراجعه کنید");
        }}>  
                  
                  <Text style={styles.textdemo}>Book a Demo</Text>
    
                </TouchableOpacity>
                        
    
    
    
                        
                    </View>
                </View>
                    
                );
            }
        }
        return infos;
    }



    render() {
        return <View style={styles.articleImggg}>
            <View style={styles.articleBody}>



            <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page4"); this.setState({auth : "", mainId : ""});
    }}>  
              <Image style={styles.card} source={require("../../assets/left-arrow(10).png")} />
              
            </TouchableOpacity>



            

            <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page15",{
                auth : this.state.auth,
                mainId : this.state.mainId,
            });
    }}>  
              
              <Image style={styles.card2} source={require("../../assets/filter.png")} /> 

            </TouchableOpacity>


            <TouchableOpacity onPress={() => { alert("قابلیت چت به زودی اضافه خواهد شد");
    }}>  
              
              <Image style={styles.card3} source={require("../../assets/notification.png")} />
            </TouchableOpacity>


            
            
            </View>
            <ScrollView style = {styles.Page}>

                {this.state.aks}

            {this.showActivityList()}



            


            </ScrollView>
            <View style={styles.articleBody2}>
                <Image style={styles.articleImg} source={require("../../assets/Group2.png")}/>




                

                <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page14",{
                auth : this.state.auth,
                mainId : this.state.mainId,
            });
    }}>  
              
              <Image style={styles.articleImg} source={require("../../assets/exams.png")}/>

            </TouchableOpacity>


            <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page24",{
                auth : this.state.auth,
                mainId : this.state.mainId,
            });
    }}>  
              
              <Image style={styles.articleImg} source={require("../../assets/course.png")}/>

            </TouchableOpacity>


                
                


                <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page17");
    }}>  
              <Image style={styles.articleImg} source={require("../../assets/profile.png")}/>
              

            </TouchableOpacity>



                </View>
                
            
        </View>
        
    }
}


const styles = StyleSheet.create({
    babaGhori : {
        width : responsiveWidth(10),
        height : responsiveHeight(5),
        marginLeft : responsiveWidth(45)
    },
    TextTitle: {
        fontSize: 35,
       
    },
    fonttext1: {
        fontSize: responsiveFontSize(2),
        marginLeft:responsiveWidth(5)
       
    },
    fonttext2: {
        fontSize: responsiveFontSize(1.5),
        marginTop : responsiveHeight(10)
       
    },
    fonttext3: {
        fontSize: responsiveFontSize(2),
        marginRight:responsiveWidth(20)
       
    },
    fonttext4: {
        fontSize: responsiveFontSize(1.5),
        marginRight:responsiveWidth(20),
        marginLeft:responsiveWidth(15),
        marginTop:responsiveHeight(2)
    },
    fonttext5: {
        fontSize: responsiveFontSize(2),
        marginLeft:responsiveWidth(15)
       
    },
    fonttext6: {
        fontSize: responsiveFontSize(1.8),
        marginRight:responsiveWidth(5),     
    },

    inside1: {
        flexDirection:"column",
        width:responsiveWidth(40),
        height:responsiveHeight(0)
         
    },
    inside2: {
        flexDirection:"column",
        height:responsiveHeight(0),
        width : responsiveWidth(35)
       
    },
    outer: {
        flexDirection:"row",  
    },

    outer2: {
        flexDirection:"row",
        marginTop:responsiveHeight(10)  
    },

    container: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: -30,
        marginBottom:responsiveHeight(2)        
      },
      
      input: {
        width: responsiveWidth(80),
        height: 44,
        padding: 10,
        marginTop: responsiveHeight(3),
        backgroundColor: '#ffffff',
        marginRight: responsiveWidth(10)
      },
    Page: {
        width: responsiveWidth(100),
        height:responsiveHeight(78),
        backgroundColor:"#f1f0f0"
    },

    Page2: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: '#fdc254',
    },
    title: {
        fontSize: 25,
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(2)
    },

    title2: {
        fontSize: 25,
        marginRight: responsiveWidth(45),
        marginTop: responsiveHeight(2)
    },
    Digi: { 
        height: responsiveHeight(5), 
        borderColor: 'gray', 
        borderWidth: 1,
        width:responsiveWidth(70), 
        marginLeft:responsiveWidth(15),
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop:responsiveHeight(1),
        marginLeft:responsiveWidth(3)
    },

    kala2: {
        height: responsiveHeight(5), 
        borderColor: 'gray', 
        borderWidth: 1,
        width:responsiveWidth(70), 
        marginLeft:responsiveWidth(15),
        marginTop: responsiveHeight(5)
    },

    card: {
       marginLeft:responsiveWidth(5),
    },
    card2: {
        marginLeft: responsiveWidth(70)
     },
     card3: {
        marginLeft:responsiveWidth(5),
        marginTop: -2
     },
     card4: {
        borderRadius: 50,
        height:responsiveHeight(5),
        width:responsiveWidth(10),
        marginLeft:responsiveWidth(5)
        
     },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        flexDirection: "row",
        marginTop:responsiveHeight(2),
        height:responsiveHeight(10),
        color:"#f1f0f0"
    },
    articleBody2: {
        flexDirection: "row",
        height:responsiveHeight(10),
    },

    articleImg: {
        marginLeft:responsiveWidth(13.5)
    },
    articleImgg: {
        flexDirection:"column",
        width:responsiveWidth(80),
        backgroundColor:"#ffffff",
        height:responsiveHeight(30),
        marginLeft:responsiveWidth(10)
    },

    article2: {
        flexDirection:"column",
        width:responsiveWidth(80),
        backgroundColor:"#ffffff",
        height:responsiveHeight(30),
        marginLeft:responsiveWidth(10),
        marginTop:responsiveHeight(5)
    },
    articleImggg: {
        color:"#f1f0f0"
    },
    blueBody: {
        flexDirection: "row",
        height:responsiveHeight(8),
        marginTop:responsiveHeight(12),
        backgroundColor:"#285fd7"
    },
    textdemo: {
        color:"#ffffff",
        fontSize:responsiveFontSize(3),
        marginTop:responsiveHeight(1.5),
        marginLeft:responsiveWidth(22)
    },

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
			//Getbook: getbook
			//GetCat:getcat
        },
        dispatch
    );

    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(test2);
    