import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView} from "react-native"

import React from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"
import Axios from "axios"



import getbook from "./../../Action/get_book"

import {connect} from "react-redux"

import {bindActionCreators} from "redux"


 class test extends React.Component {

    state = {
        names: [],
        desc: [],
        summary: [],
        publisher: [],
        price:[],
        id:[],
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }


     componentDidMount() {
        //const bookss = await Axios.get('http://reactnative.redbees.ir/books/getall');
      

        // alert(bookss.data[0].title);
        
        // for(var i = 0;i < bookss.data.length;i++) {
        //     var book = this.state.names.concat(bookss.data[i].title);
        //     var descc = this.state.desc.concat(bookss.data[i].writer);
        //     var sum = this.state.summary.concat(bookss.data[i].summary);
        //     var pub = this.state.publisher.concat(bookss.data[i].publisher);
        //     var pri = this.state.price.concat(bookss.data[i].price);
        //     var idd = this.state.id.concat(bookss.data[i].id);
        //     this.setState({names: book,desc: descc,summary: sum,publisher: pub,price: pri,id: idd});

            this.props.Getbook();
        

       // alert(this.state.names[0]);

    }




    sendItemsInfo = () => {
        let infos = [];
        for (var i = 0; i < this.state.names.length; i++) {
            let a = this.state.names[i];
            let b = this.state.desc[i];
            let c = this.state.summary[i];
            let d = this.state.publisher[i];
            let e = this.state.price[i];
            let f = this.state.id[i]; 
           

            infos.push(
                <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
                navigate("page4",{
                    name:a,
                    desc:b,
                    sum:c,
                    publ:d,
                    pri:e,
                    id:f,
                });
        }}>  
                    <Text style = {styles.TextTitle}>{this.state.names[i]}</Text>
                    {/* <Text style = {styles.TextTitle}>next</Text> */}
                </TouchableOpacity>);
        }
        return infos;
    }






    


    render() {
        // return <ScrollView style={styles.Page}>
        //     <View style={styles.title}>
        //         <Text style={styles.Digi}>Digikala</Text>
        //     </View>
           
        // {/* {this.sendItemsInfo()}  */}

        // <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
        //         navigate("page4");
        // }}>  
        //             {/* <Text style = {styles.TextTitle}>{this.state.names[i]}</Text> */}
        //             <Text style = {styles.TextTitle}>next</Text>
        //         </TouchableOpacity>

        // </ScrollView>


        return <ScrollView style={styles.Page}>

        <Image style={styles.articleImg} source={require("../../assets/121.png")} />
        {/* <Image style={styles.articleImgg} source={require("../../assets/Illustration1.png")} /> */}

        <Image style={styles.articleImgg} source={require("../../assets/Dot.png")} />


        <Text style = {styles.title} >ثبت نام کن!</Text>
       
    {/* {this.sendItemsInfo()}  */}

    <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page2");
    }}>  
               
               <Image style={styles.articleImggg} source={require("../../assets/Button.png")} />  
            </TouchableOpacity>

    </ScrollView>




        
    }
}


const styles = StyleSheet.create({

    TextTitle: {
        fontSize:responsiveFontSize(5),
        marginLeft:responsiveWidth(5),
        borderBottomColor:"black",
        borderBottomWidth:1,
    },

    Page: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: 'white',
    },

    title: {
        marginRight: responsiveWidth(36),
        marginTop: responsiveHeight(4),
        fontSize: 30,
    },

    Digi: {
        color: "white",
        fontSize: responsiveFontSize(5),
        marginLeft:responsiveWidth(63)
    },

    kala: {
        color: 'black',
        textAlign: "center",
        fontSize: responsiveFontSize(5),
        marginTop:responsiveHeight(10),
    },

    card: {
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(67),
        marginBottom: responsiveHeight(3),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        // width
        marginLeft: responsiveWidth(4),
        fontSize: responsiveFontSize(2),
        color: "#EBEBEB"
    },

    articleImg: {
        width: responsiveWidth(100),
        height: responsiveHeight(100),
        marginTop: responsiveHeight(-15),
        
    },
    articleImgg: {
        marginLeft: responsiveWidth(40),
        marginTop: responsiveHeight(-17),
    },
    articleImggg: {
        marginLeft: responsiveWidth(35),
        marginTop: responsiveHeight(5),
    }

})




const mapStateToProps = state => ({
    books: state.book
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
			Getbook: getbook
			//GetCat:getcat
        },
        dispatch
    );

    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(test);