import {Image, StyleSheet, Text, View, TouchableOpacity, ScrollView,AppRegistry} from "react-native"

import React from "react"

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"



import getbook from "./../../Action/get_book"

import {connect} from "react-redux"

import {bindActionCreators} from "redux"




 class test2 extends React.Component {

    state = {
        names: [],
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        //let infos = [];
        // infos[0] = this.props.navigation.getParam("name");
        // infos[1] = this.props.navigation.getParam("desc");
        // infos[2] = this.props.navigation.getParam("sum");
        // infos[3] = this.props.navigation.getParam("publ");
        // infos[4] = this.props.navigation.getParam("pri");
        // infos[5] = this.props.navigation.getParam("id");
        // infos[6] = "http://reactnative.redbees.ir/books/file/" + this.props.navigation.getParam("id");
        // alert(this.props.books.data[0].PublishDate);
        

        // return infos;
    }



    render() {
        return <ScrollView style={styles.Page}>
            
           
            <Image style={styles.articleImg} source={require("../../assets/golombe.png")} />
            <Image style={styles.articleImgg} source={require("../../assets/Dot2.png")} />

            <Text style = {styles.title} >یاد بگیر</Text>

            <TouchableOpacity onPress={() => { const {navigate} = this.props.navigation;
            navigate("page3");
    }}>  
               <Image style={styles.articleImggg} source={require("../../assets/Button.png")} />
               
            </TouchableOpacity>

            

        </ScrollView>
        
    }
}


const styles = StyleSheet.create({

    TextTitle: {
        fontSize:responsiveFontSize(5),
        marginLeft:responsiveWidth(5),
        borderBottomColor:"black",
        borderBottomWidth:1,
    },

    Page: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: 'white',
    },

    title: {
        marginRight: responsiveWidth(40),
        marginTop: responsiveHeight(4),
        fontSize: 30,
    },

    Digi: {
        color: "white",
        fontSize: responsiveFontSize(5),
        marginLeft:responsiveWidth(63)
    },

    kala: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(5),
        marginTop:responsiveHeight(1),
        marginLeft:responsiveWidth(3)
    },

    kala2: {
        color: 'black',
        textAlign: "left",
        fontSize: responsiveFontSize(3),
        marginTop:responsiveHeight(3),
        marginLeft:responsiveWidth(3)
    },

    card: {
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(67),
        marginBottom: responsiveHeight(3),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        // width
        marginLeft: responsiveWidth(4),
        fontSize: responsiveFontSize(2),
        color: "#EBEBEB"
    },

    articleImg: {
        width: responsiveWidth(100),
        height: responsiveHeight(90),
        marginTop: responsiveHeight(-25),
    },
    articleImgg: {
        marginLeft: responsiveWidth(42),
        marginTop: responsiveHeight(2),

    },
    articleImggg: {
        marginLeft: responsiveWidth(35),
        marginTop: responsiveHeight(5),
    }

})


// const mapStateToProps = state => ({
//     books = state.book 
// })

const mapStateToProps = state => ({
    books: state.book
});


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
			Getbook: getbook
			//GetCat:getcat
        },
        dispatch
    );

    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(test2);
    