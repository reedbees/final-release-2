

import { Image, StyleSheet, Text, View, TouchableOpacity } from "react-native"
import React from "react"
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions";


export default class Item extends React.Component {
    constructor(props) {
        super(props);
    }



    


    render() {
        return <View style={styles.card}>
            <Text style={styles.articleTitle}>{this.props.name}</Text>
            <View style={{ flexDirection: "row" }}>
                <View style={{ flexDirection: "column" }}>
                    <Text style={styles.articleBody}>{"Price: " + this.props.price + "$"}</Text>
                    <Text style={styles.articleBody}>{"Description: " + this.props.description}</Text>
                </View>
                {this.props.picLink}
            </View>
        </View>
    }
}


const styles = StyleSheet.create({
    card: {
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(67),
        marginBottom: responsiveHeight(3),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "black",
    },

    articleBody: {
        // width
        marginLeft: responsiveWidth(4),
        fontSize: responsiveFontSize(2),
        color: "black"
    },

    articleImg: {
        marginRight: responsiveWidth(2),
        marginLeft: responsiveWidth(5),
        width: responsiveWidth(60),
        height: responsiveHeight(20),
    }
})