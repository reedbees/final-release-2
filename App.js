/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */




import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView } from "react-native"
import React from "react"
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions"

import Item from "./Components/Item"

import Main from "./App/MainPage/Main"
import "react-native-gesture-handler"
import ps4 from "./App/Items/ps4"

import { createAppContainer } from "react-navigation"
import { createStackNavigator } from "react-navigation-stack"
import test from "./App/test/test"
import test2 from "./App/test/test2"
import Book from "./App/Reducer/Book"
import test3 from "./App/test/test3"
import login1 from "./App/test/login1"
import loginstudent1 from "./App/logins/loginstudent1"
import loginstudent2 from "./App/logins/loginstudent2"
import registerstudent1 from "./App/register/registerstudent1"
import registerstudent2 from "./App/register/registerstudent2"
import techerlogin from "./App/teacher/techerlogin"
import loginteacher2 from "./App/teacher/loginteacher2"
import firstsawstudent from "./App/inApp/firstsawstudent"
import bookdemo from "./App/inApp/bookdemo"
import justtest from "./App/practical/justtest"
import exam1 from "./App/exams/exam1"
import seesubject from "./App/subject/seesubject"
import showteachersubject from "./App/subject/showteachersubject"
import testimage from "./App/testimage/testimage"
import subjectregister from "./App/subject/subjectregister"
import showexam from "./App/exams/showexam"
import previewExamScore from "./App/exams/previewExamScore"
import examResualt from "./App/exams/examResualt"
import examRProgress from "./App/exams/examRProgress"
import examFilter from "./App/exams/examFilter"
import course1 from "./App/course/course1"
import firstSawTeacher from "./App/inAppTeacher/firstSawTeacher"

import AsyncStorage from "@react-native-community/async-storage"

import { applyMiddleware, createStore } from "redux"

import { Provider } from "react-redux"

import reducers from "./Reducer/Index"

import thunk from "redux-thunk"

import { persistReducer, persistStore as persistStoreRaw } from "redux-persist"

import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2"
import examCreator from "./App/exams/examCreator"
import courseCreator from "./App/course/courseCreator"
import courseSubject from "./App/course/courseSubject"
import showCourseSubject from "./App/subject/showCourseSubject"
import courseToClass from "./App/course/courseToClass"
import beforCreate from "./App/course/beforCreate"
import mainExamCreator from "./App/exams/mainExamCreator"
import questionCreator from "./App/exams/questionCreator"
import changeExams from "./App/exams/changeExams"
import deleteExams from "./App/exams/deleteExams"
import mainDeleteExam from "./App/exams/mainDeleteExam"
import showExamToDelete from "./App/exams/showExamToDelete"
import signTeacher from "./App/signup/signTeacher"
import signStudent from "./App/signup/signStudent"
import seeCourseToCreate from "./App/exams/seeCourseToCreate"
import seeStudentToCreate from "./App/exams/seeStudentToCreate"
import seeCourse from "./App/course/seeCourse"

const persistConfig = {
    key: "root",
    storage: AsyncStorage,
    stateReconciler: autoMergeLevel2
};

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStoreWithMiddleware(persistedReducer);



const persistStore = storeToPersist =>
    new Promise(resolve => {
        persistStoreRaw(storeToPersist, undefined, () => {
            registerScreens(storeToPersist, Provider);
            resolve();
        });
    });



const PushRouteOne = createStackNavigator({
    page1: {
        screen: test,
    },
    page2: {
        screen: test2,
    },
    page3: {
        screen: test3,
    },
    page4: {
        screen: login1
    },
    page5: {
        screen: loginstudent1
    },
    page6: {
        screen: loginstudent2
    },
    page7: {
        screen: registerstudent1
    },
    page8: {
        screen: registerstudent2
    },
    page9: {
        screen: techerlogin
    },
    page10: {
        screen: loginteacher2
    },
    page11: {
        screen: firstsawstudent
    },
    page12: {
        screen: bookdemo
    },
    page13: {
        screen: justtest
    },
    page14: {
        screen: exam1
    },
    page15: {
        screen: seesubject
    },
    page16: {
        screen: showteachersubject
    },
    page17: {
        screen: testimage
    },
    page18: {
        screen: subjectregister
    },
    page19: {
        screen: showexam
    },
    page20: {
        screen: previewExamScore
    },
    page21: {
        screen: examResualt
    },
    page22: {
        screen: examRProgress
    },
    page23: {
        screen: examFilter
    },
    page24: {
        screen: course1
    },
    page25: {
        screen: firstSawTeacher
    },
    page26: {
        screen: examCreator
    },
    page27: {
        screen: courseCreator
    },
    page28: {
        screen: courseSubject
    },
    page29: {
        screen: showCourseSubject
    },
    page30: {
        screen: courseToClass
    },
    page31: {
        screen: beforCreate
    },
    page32: {
        screen: mainExamCreator
    },
    page33: {
        screen: questionCreator
    },
    page34: {
        screen: changeExams
    },
    page35: {
        screen: deleteExams
    },
    page36 : {
        screen : mainDeleteExam
    },
    page37 : {
        screen : showExamToDelete
    },
    page38 : {
        screen : signTeacher
    },
    page39 : {
        screen : signStudent
    },
    page40 : {
        screen : seeCourseToCreate
    },
    page41 : {
        screen : seeStudentToCreate
    },
    page42 : {
        screen : seeCourse
    }
}, {
    initialRouteName: "page1",
    mode: "modal",
    headerMode: "none",
})

const AppContainer = createAppContainer(PushRouteOne)


export default class App extends React.Component {



    render() {
        // return <ScrollView style={styles.Page}>
        //     <View style={styles.title}>
        //         <Text style={styles.Digi}>Digikala</Text>
        //     </View>

        //     {this.sendItemsInfo()}

        // </ScrollView>



        return <Provider store={store}>
            <AppContainer />
        </Provider>

        // return <AppContainer/>
    }
}


const styles = StyleSheet.create({

    Page: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: 'white',
    },

    title: {
        flexDirection: "row",
        backgroundColor: "red",
        height: responsiveHeight(10),
    },

    Digi: {
        color: "white",
        fontSize: responsiveFontSize(5),
        marginLeft: responsiveWidth(63)
    },

    kala: {
        color: '#FAFAFA',
        textAlign: "center",
        fontSize: responsiveFontSize(5),
    },

    card: {
        marginLeft: responsiveWidth(3),
        marginRight: responsiveWidth(67),
        marginBottom: responsiveHeight(3),
    },

    articleTitle: {
        // marginLeft: responsiveWidth(3),
        width: responsiveWidth(100),
        fontSize: responsiveFontSize(3),
        color: "white",
    },

    articleBody: {
        // width
        marginLeft: responsiveWidth(4),
        fontSize: responsiveFontSize(2),
        color: "#EBEBEB"
    },

    articleImg: {
        marginRight: responsiveWidth(2),
        marginLeft: responsiveWidth(5),
        width: responsiveWidth(60),
        height: responsiveHeight(20),
    }

})