import {GET_CAT} from "./../Types/type";

export default (state = null, action) => {
    switch (action.type) {
        case GET_CAT:
            state = action.payload;
            return {
                data: state,
                alert: false
            };
        
        default:
            return state;
    }
};