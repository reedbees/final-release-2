import { combineReducers } from "redux";
import BookReducer from "./book";
import CatReducer from "./cat";


const rootReducer = combineReducers({
    book : BookReducer,
    cat:CatReducer
});

export default rootReducer;
