import {GET_BOOK} from "./../Types/type";

export default (state = null, action) => {
    switch (action.type) {
        case GET_BOOK:
            state = action.payload;
            return {
                data: state,
                alert: false
            };
        
        default:
            return state;
    }
};